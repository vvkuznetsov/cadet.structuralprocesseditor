﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Diagnostics;
using StructuralProcessEditor.CommonStructuralProcessController;
using System.Windows.Forms;
using System.Drawing;

namespace StructuralProcessEditor.DiagrammExtension
{
    abstract class InterfaceMarker : Marker
    {
        static InterfaceMarker()
        {
            defaultSize = 10;
        }

        public override void UpdateLocation()
        {
            var fig = targetFigure as SolidFigure;
            if (fig != null)
            {
                if (location.X > fig.Bounds.Right)
                    location.X = (int)fig.Bounds.Right;
                else if (location.X < fig.Bounds.Left)
                    location.X = (int)fig.Bounds.Left;

                if (location.Y > fig.Bounds.Bottom)
                    location.Y = (int)fig.Bounds.Bottom;
                else if (location.Y < fig.Bounds.Top)
                    location.Y = (int)fig.Bounds.Top;
            }
        }

        /// <summary>
        /// Ограничивает возможное местоположение маркера относительно связанной фигуры
        /// </summary>
        public virtual void RestrictPossibleLocation()
        {
            var fig = targetFigure as SolidFigure;
            if (fig != null)
            {
                if (location.X > fig.Bounds.Right)
                    location.X = (int)fig.Bounds.Right;
                else if (location.X < fig.Bounds.Left)
                    location.X = (int)fig.Bounds.Left;

                if (location.Y > fig.Bounds.Bottom)
                    location.Y = (int)fig.Bounds.Bottom;
                else if (location.Y < fig.Bounds.Top)
                    location.Y = (int)fig.Bounds.Top;
            }
        }
    }
}
