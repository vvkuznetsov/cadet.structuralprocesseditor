﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ObjectAttributeMapping.OAM.OAMController;

namespace StructuralProcessEditor.DiagrammExtension
{
    interface IGraphicalStateable
    {
        void SaveGraphicalState(UnitOfWorkWithOAM uow);
        void RestoreGraphicalState(UnitOfWorkWithOAM uow);
    }
}
