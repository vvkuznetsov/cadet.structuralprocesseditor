﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Diagnostics;
using System.Drawing;

namespace StructuralProcessEditor.DiagrammExtension
{
    class InputInterfaceMarker : InterfaceMarker
    {
        public override void Draw(System.Drawing.Graphics gr)
        {
            //gr.DrawRectangle(Pens.Black, location.X - defaultSize, location.Y - defaultSize, defaultSize * 2, defaultSize * 2);
            //gr.DrawLines(Pens.Black,
            //    new Point[3] 
            //    { 
            //        new Point(location.X - defaultSize, location.Y + defaultSize),
            //        new Point(location.X, location.Y + 3 *  defaultSize),
            //        new Point(location.X + defaultSize, location.Y + defaultSize),
            //    });
            gr.DrawEllipse(Pens.Black, location.X - defaultSize, location.Y - defaultSize, defaultSize * 2, defaultSize * 2);
        }

        public override RectangleF Bounds
        {
            get
            {
                var rect = new RectangleF(location.X - defaultSize, location.Y - defaultSize, 2 * defaultSize, 2 * defaultSize);
                return rect;
            }
        }

        public override bool IsInsidePoint(Point p)
        {
            if (p.X >= location.X - defaultSize && p.X < location.X + defaultSize
                && p.Y >= location.Y - defaultSize && p.Y < location.Y + defaultSize)
                return true;
            return false;
        }
    }
}
