﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Diagnostics;

namespace StructuralProcessEditor.DiagrammExtension
{
    class StructureRepresentation : RectFigure
    {
        private List<InterfaceMarker> _interfaces;

        public List<InterfaceMarker> Interfaces
        {
            get 
            {
                if (_interfaces == null)
                    _interfaces = new List<InterfaceMarker>();
                return _interfaces;
            }
        }

        public override List<Marker> CreateMarkers(Diagram diagram)
        {
            var res = base.CreateMarkers(diagram);
            foreach (var m in Interfaces)
            {
                res.Add(m);
            }
            return res;
        }
    }
}
