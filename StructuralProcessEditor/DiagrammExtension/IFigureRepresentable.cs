﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Diagnostics;

namespace StructuralProcessEditor.DiagrammExtension
{
    /// <summary>
    /// Интерфейс сущности, обладающей графическим представлением в виде фигуры
    /// </summary>
    interface IFigureRepresentable
    {
        /// <summary>
        /// Графическое представление
        /// </summary>
        Figure Representation
        {
            get;
            set;
        }
    }
}
