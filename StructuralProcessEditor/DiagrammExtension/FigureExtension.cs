﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StructuralProcessEditor.StructuralDiagramms;

namespace StructuralProcessEditor.DiagrammExtension
{
    /// <summary>
    /// Интерфейс объекта, связанного с элементом структурной диаграммы
    /// </summary>
    public interface IStructuralElement
    {
        StructuralDiagrammElement StructuralElement { get; set; }
    }
}
