﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StructuralProcessEditor.Diagramms;
using Diagnostics;

namespace StructuralProcessEditor.DiagrammExtension
{
    /// <summary>
    /// Интерфейс объекта контейнера элементов
    /// </summary>
    interface IElementsContainer
    {
        LogicalDiagrammElement FindElementByGraphicalRepresentation(Figure figure);
    }
}
