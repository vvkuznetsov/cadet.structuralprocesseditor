﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace StructuralProcessEditor.DiagrammExtension
{
    class OutputInterfaceMarker : InterfaceMarker
    {
        public override void Draw(System.Drawing.Graphics gr)
        {
            gr.DrawRectangle(Pens.Black, location.X - defaultSize, location.Y - defaultSize, defaultSize * 2, defaultSize * 2);
            //gr.DrawLines(Pens.Black,
            //    new Point[5] 
            //    { 
            //        new Point(location.X - defaultSize, location.Y + defaultSize),
            //        new Point(location.X - defaultSize, location.Y + 3 * defaultSize),
            //        new Point(location.X, location.Y + defaultSize),
            //        new Point(location.X + defaultSize, location.Y + 3 * defaultSize),
            //        new Point(location.X + defaultSize, location.Y + defaultSize),
            //    });
        }

        public override RectangleF Bounds
        {
            get
            {
                var rect = new RectangleF(location.X - defaultSize, location.Y - defaultSize, 2 * defaultSize, 2 * defaultSize);
                return rect;
            }
        }

        public override bool IsInsidePoint(Point p)
        {
            if (p.X >= location.X - defaultSize && p.X < location.X + defaultSize
                && p.Y >= location.Y - defaultSize && p.Y < location.Y + defaultSize)
                return true;
            return false;
        }
    }
}
