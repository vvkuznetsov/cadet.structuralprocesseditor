﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Diagnostics;

namespace StructuralProcessEditor.DiagrammExtension
{
    class StructuralConnectionFreesideMarker : EndLineMarker
    {
        public StructuralConnectionFreesideMarker(Diagram dia, int pointIndex):
            base(dia, pointIndex)
        {
            diagram = dia;
        }

        public override void Offset(int dx, int dy)
        {
            base.Offset(dx, dy);
        }

        /// <summary>
        /// Определяет политику игонрирования объектов, связь с которыми не может быть установлена
        /// </summary>
        /// <param name="connectionSource">источник соединения</param>
        /// <param name="currentClosestObject">текущий ближайший объект</param>
        /// <returns>флаг игнорирования</returns>
        protected override bool DoIgnoreObject(SolidFigure connectionSource, SolidFigure currentClosestObject)
        {
            bool ignore;

            var srcType = connectionSource.GetType();
            var currentClosestObjectType = currentClosestObject.GetType();
            if (srcType == typeof(InputInterfaceMarker) && currentClosestObjectType == typeof(OutputInterfaceMarker)
                || srcType == typeof(OutputInterfaceMarker) && currentClosestObjectType == typeof(InputInterfaceMarker))
            {
                ignore = false;
            }
            else
            {
                ignore = true;
            }

            return ignore;
        }
    }
}
