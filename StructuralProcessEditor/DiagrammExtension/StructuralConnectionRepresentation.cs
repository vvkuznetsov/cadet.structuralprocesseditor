﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Diagnostics;
using System.ComponentModel;

namespace StructuralProcessEditor.DiagrammExtension
{
    /// <summary>
    /// Представление соединения между структурными элементами
    /// </summary>
    class StructuralConnectionRepresentation : LedgeLineFigure, INotifyPropertyChanged
    {
        public override List<Marker> CreateMarkers(Diagram diagram)
        {
            RecalcPath();
            List<Marker> markers = new List<Marker>();
            EndLineMarker m2 = new StructuralConnectionFreesideMarker(diagram, 1);
            m2.targetFigure = this;
            LedgeMarker m3 = new LedgeMarker();
            m3.targetFigure = this;
            m3.UpdateLocation();

            markers.Add(m2);
            markers.Add(m3);

            return markers;
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
