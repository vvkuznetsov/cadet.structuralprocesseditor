﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Diagnostics;
using System.Windows.Forms;
using StructuralProcessEditor.CommonStructuralProcessController;
using StructuralProcessEditor.DiagrammExtension;
using ObjectAttributeMapping.OAM.OAMController;

namespace StructuralProcessEditor.Diagramms
{
    /// <summary>
    /// Базовый класс диаграмм
    /// </summary>
    public abstract class LogicalDiagramm : Logical, IContextMenuable, IElementsContainer, IGraphicalStateable
    {
        /// <summary>
        /// Графическое представление диаграммы
        /// </summary>
        private Diagram _diagramm;

        #region IContextMenuable

        public virtual ContextMenuStrip ContextMenu
        {
            get { return null; }
        }

        #endregion IContextMenuable

        public Diagram Representation
        {
            get 
            {
                if (_diagramm == null)
                    _diagramm = new Diagram();
                return _diagramm;
            }
        }

        #region IElementsContainer

        public virtual LogicalDiagrammElement FindElementByGraphicalRepresentation(Figure figure)
        {
            return null;
        }

        #endregion IElementsContainer

        public virtual void SaveGraphicalState(UnitOfWorkWithOAM uow)
        {
        }

        public virtual void RestoreGraphicalState(UnitOfWorkWithOAM uow)
        {
        }
    }
}
