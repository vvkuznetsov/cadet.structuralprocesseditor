﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StructuralProcessEditor.CommonStructuralProcessController;
using System.Windows.Forms;
using StructuralProcessEditor.DiagrammExtension;
using Diagnostics;
using ObjectAttributeMapping.OAM.OAMDB;
using ObjectAttributeMapping.OAM.OAMController;
using StructuralProcessEditor.View;
using StructuralProcessEditor.ViewModel;

namespace StructuralProcessEditor.Diagramms
{
    public class LogicalDiagrammElement : Logical, IContextMenuable, IElementsContainer, IObjectable, IGraphicalStateable
    {
        #region IContextMenuable

        private ContextMenuStrip _contextMenu;
        public virtual ContextMenuStrip ContextMenu
        {
            get { return _contextMenu; }
        }

        #endregion IContextMenuable

        #region IElementsContainer

        public virtual LogicalDiagrammElement FindElementByGraphicalRepresentation(Figure figure)
        {
            return null;
        }

        #endregion IElementsContainer

        #region IObjectable



        #endregion IObjectable

        #region .ctor

        public LogicalDiagrammElement() 
        {
            _contextMenu = new ContextMenuStrip();
        }

        public LogicalDiagrammElement(TObject obj) : this()
        {
            Entity = obj;
        }

        #endregion .ctor

        public virtual void SaveGraphicalState(UnitOfWorkWithOAM uow)
        {
        }

        public virtual void RestoreGraphicalState(UnitOfWorkWithOAM uow)
        {
        }
    }
}