﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ObjectAttributeMapping.OAM.OAMDB;
using ObjectAttributeMapping.OAM.OAMController;
using StructuralProcessEditor.ViewModel;
using StructuralProcessEditor.View;

namespace StructuralProcessEditor.Diagramms
{
    public  class Logical
    {
        /// <summary>
        /// Объект некоторого типа
        /// </summary>
        private TObject _entity;
        public TObject Entity
        {
            get
            {
                return _entity;
            }
            set
            {
                _entity = value;
            }
        }

        /// <summary>
        /// Метод для привязки объекта к графическому представлению
        /// </summary>
        /// <returns></returns>
        public bool AssignEntity(TType defType = null)
        {
            bool result = false;
            using (var uow = new UnitOfWorkWithOAM())
            {
                var objVM = new ObjectVM();
                objVM.UOW = uow;

                //Окошко привязки объекта
                var connectWithObjectDialog = new ConnectWithObjectDialog() { DataContext = objVM };
                result = connectWithObjectDialog.ShowDialog().Value;

                if (result && objVM.Type != null)
                {
                    var type = uow.Context.TTypes.Where((e) => e.Id == objVM.Type.Id).FirstOrDefault();
                    if (type != null)
                    {
                        Entity = type.CreateObject(uow);
                        uow.Commit();
                        
                    }

                    //Окошко редактирования атрибутов
                    objVM = new ObjectVM(Entity, objVM.Type) { UOW = uow };
                    var objectView = new ObjectView() { DataContext = objVM};
                    bool resAttr = objectView.ShowDialog().Value;
                    if (resAttr)
                    { 
                        //Обработать!
                    }

                }
            }
            return result;
        }
    }
}
