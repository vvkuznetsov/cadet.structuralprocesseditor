﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ObjectAttributeMapping.OAM.OAMDB;
using Diagnostics;
using StructuralProcessEditor.DiagrammExtension;
using StructuralProcessEditor.Diagramms;
using ObjectAttributeMapping.OAM.OAMController;
using StructuralProcessEditor.CommonStructuralProcessController;

namespace StructuralProcessEditor.StructuralDiagramms
{
    /// <summary>
    /// Соединение
    /// </summary>
    public class StructuralConnection : StructuralDiagrammElement, IFigureRepresentable
    {
        public StructuralInterface HeadInterface { get; set; }

        public StructuralInterface TailInterface { get; set; }

        private Figure _graphicsElement;

        public override void SaveGraphicalState(UnitOfWorkWithOAM uow)
        {

        }

        #region IFigureRepresentable

        public Figure Representation
        {
            get
            {
                return _graphicsElement;
            }
            set
            {
                _graphicsElement = value;
            }
        }

        #endregion IFigureRepresentable

        #region .ctor

        public StructuralConnection(StructuralDiagramm dia)
            :base(dia)
        {
            using (var uow = new UnitOfWorkWithOAM())
            {
                baseElementType = uow.Context.TTypes.FirstOrDefault(t => t.Name == TType.TypeNameConnection);
            }
        }

        #endregion .ctor
    }
}
