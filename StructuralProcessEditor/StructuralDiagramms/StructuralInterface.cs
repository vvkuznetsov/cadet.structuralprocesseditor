﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Diagnostics;
using ObjectAttributeMapping.OAM.OAMDB;
using StructuralProcessEditor.DiagrammExtension;
using StructuralProcessEditor.Diagramms;
using StructuralProcessEditor.CommonStructuralProcessController;
using System.Windows.Forms;
using System.ComponentModel;
using ObjectAttributeMapping.OAM.OAMController;

namespace StructuralProcessEditor.StructuralDiagramms
{
    /// <summary>
    /// Интерфейс
    /// </summary>
    public class StructuralInterface : StructuralDiagrammElement, IFigureRepresentable
    {
        /// <summary>
        /// Связанные соединения
        /// </summary>
        private List<StructuralConnection> _attachedConnections;

        public List<StructuralConnection> Connections
        {
            get 
            {
                if (_attachedConnections == null)
                    _attachedConnections = new List<StructuralConnection>();
                return _attachedConnections;
            }
        }

        #region IFigureRepresentable

        protected Figure _graphicsElement;
        public virtual Figure Representation
        {
            get
            {
                return _graphicsElement;
            }
            set
            {
                _graphicsElement = value;
            }
        }

        #endregion IFigureRepresentable

        protected virtual void ShowInfo(object sender, EventArgs e)
        { }

        public virtual void AddConnection(object sender, EventArgs e)
        {
            var connection = new StructuralConnection(Owner);

            var marker = new StructuralConnectionFreesideMarker(Owner.Representation, 0);
            marker.location = (Representation as InterfaceMarker).location;
            marker.location.Offset(0, (int)(Representation as InterfaceMarker).Size.Height / 2 + StructureRepresentation.DefaultSize);

            var gc = GraphicsController.CreateConnectionGraphicsRepresentation(Representation as InterfaceMarker, marker);
            gc.PropertyChanged += PropertyChangedHandler;

            connection.Representation = gc;
            connection.HeadInterface = this;
            
            Connections.Add(connection);
            Owner.AddConnection(connection);
        }

        protected void PropertyChangedHandler(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == LineFigure.PropertyNameTo)
            {
                var connRepr = sender as LineFigure;
                if (connRepr == null)
                    return;                
                var targetIfaceMarker = connRepr.To as InterfaceMarker;
                if (targetIfaceMarker == null)
                    return;                    

                StructuralConnection connection = null;
                foreach (var c in Connections)
                {
                    if (c.Representation == connRepr)
                    {
                        connection = c;
                    }
                }
                if (connection == null)
                    return;

                StructuralInterface iface = null;
                
                foreach (var s in Owner.Structures)
                {
                    foreach (var i in s.InputInterfaces)
                    {
                        if (i.Representation == targetIfaceMarker)
                        {
                            iface = i;
                            break;
                        }
                    }
                    if (iface != null)
                        break;
                    foreach (var i in s.OutputInterfaces)
                    {
                        if (i.Representation == targetIfaceMarker)
                        {
                            iface = i;
                            break;
                        }
                    }
                    if (iface != null)
                        break;
                }
                connection.TailInterface = iface;
            }
        }

        private ContextMenuStrip _contextMenu;
        public override ContextMenuStrip ContextMenu
        {
            get
            {
                return _contextMenu;
            }
        }

        public override void SaveGraphicalState(UnitOfWorkWithOAM uow)
        {
            var fig = (Representation as SolidFigure);
            if (fig == null)
                return;

            var ent = TObject.GetObject(uow, Entity.Id);
            if (ent == null)
                return;

            var posX = ent.GetAttributeAsScalarValue(TType.TypeInterfaceAttrNamePositionX);
            var posY = ent.GetAttributeAsScalarValue(TType.TypeInterfaceAttrNamePositionY);
            if (posX == null || posY == null)
                return;
                
            posX.SetValue(fig.Location.X.ToString());
            posY.SetValue(fig.Location.Y.ToString());
        }

        #region .ctor

        public StructuralInterface(StructuralDiagramm dia)
            :base(dia)
        {
            using (var uow = new UnitOfWorkWithOAM())
            {
                baseElementType = uow.Context.TTypes.FirstOrDefault(t => t.Name == TType.TypeNameInterface);
            }
            _contextMenu = base.ContextMenu;
            _contextMenu.Items.Add(new ToolStripMenuItem("Добавить соединение", null, AddConnection));
        }

        #endregion .ctor
    }
}
