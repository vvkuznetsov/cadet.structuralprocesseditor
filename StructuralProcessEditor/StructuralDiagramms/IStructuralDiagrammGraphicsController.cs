﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Diagnostics;

namespace StructuralProcessEditor.StructuralDiagramms
{
    /// <summary>
    /// Интерфейс контроллера управления графическим представлением структурных диаграмм
    /// </summary>
    public interface IStructuralDiagrammGraphicsController
    {
        SolidFigure AddStructureGraphicsRepresentation();
        void RemoveStructureGraphicsRepresentation(SolidFigure figure);

        Marker AddInputInterfaceGraphicsRepresentation();
        void RemoveInterfaceGraphicsRepresentation(Marker figure);

        Marker AddOutputInterfaceGraphicsRepresentation();

        LineFigure CreateConnectionGraphicsRepresentation(Marker from, Marker to);

        void EditDiagramm(StructuralDiagramm dia);
    }
}
