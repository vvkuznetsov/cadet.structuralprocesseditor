﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ObjectAttributeMapping.OAM.OAMDB;
using ObjectAttributeMapping.OAM.OAMController;
using Diagnostics;
using StructuralProcessEditor.DiagrammExtension;
using StructuralProcessEditor.Diagramms;
using System.Windows.Forms;
using StructuralProcessEditor.ViewModel;
using StructuralProcessEditor.CommonStructuralProcessController;

namespace StructuralProcessEditor.StructuralDiagramms
{
    /// <summary>
    /// Структура
    /// </summary>
    public class Structure : StructuralDiagrammElement, IFigureRepresentable
    {
        private List<StructuralInterface> _inputInterfaces;

        public List<StructuralInterface> InputInterfaces
        {
            get 
            {
                if (_inputInterfaces == null)
                    _inputInterfaces = new List<StructuralInterface>();
                return _inputInterfaces;
            }
        }

        /// <summary>
        /// Входные интерфейсы
        /// </summary>
        /// <param name="useCachedData">флаг использования закешированных данных</param>
        /// <returns>интерфейсы</returns>
        public List<StructuralInterface> GetInputInterfaces(bool useCachedData = true)
        {
            List<StructuralInterface> ifaces = null;
            //if (_inputInterfaces == null || reloadData)
            //{
            //    using (var uow = new UnitOfWorkWithOAM())
            //    {
            //        uow.Context.T
            //    }
            //    _inputInterfaces = 
            //}
            return ifaces;
        }

        private List<StructuralInterface> _outputInterfaces;

        public List<StructuralInterface> OutputInterfaces
        {
            get
            {
                if (_outputInterfaces == null)
                    _outputInterfaces = new List<StructuralInterface>();
                return _outputInterfaces;
            }
        }

        /// <summary>
        /// Выходные интерфейсы
        /// </summary>
        /// <param name="useCachedData">флаг использования закешированных данных</param>
        /// <returns>интерфейсы</returns>
        public List<StructuralInterface> GetOutputInterfaces(bool useCachedData = true)
        {
            return null;
        }

        /// <summary>
        /// Внутреннее представление структуры
        /// </summary>
        private StructuralDiagramm _innerDecomposition;
        
        /// <summary>
        /// Внутреннее представление структуры
        /// </summary>
        public StructuralDiagramm InnerDecomposition
        {
            get 
            {
                if (_innerDecomposition == null)
                {
                    _innerDecomposition = new StructuralDiagramm();
                    _innerDecomposition.UpperLevel = this.Owner;
                }
                return _innerDecomposition;
            }
        }        

        public override LogicalDiagrammElement FindElementByGraphicalRepresentation(Figure figure)
        {
            LogicalDiagrammElement e = null;
            if (InputInterfaces != null)
            {
                foreach (var i in InputInterfaces)
                {
                    if (i is IFigureRepresentable)
                    {
                        if (((IFigureRepresentable)i).Representation == figure)
                        {
                            e = i;
                            break;
                        }
                    }
                }
            }
            if (OutputInterfaces != null)
            {
                foreach (var o in OutputInterfaces)
                {
                    if (o is IFigureRepresentable)
                    {
                        if (((IFigureRepresentable)o).Representation == figure)
                        {
                            e = o;
                            break;
                        }
                    }
                }
            }
            return e;
        }

        #region IFigureRepresentable

        /// <summary>
        /// Графическое представление
        /// </summary>
        private Figure _graphicsElement;

        public Figure Representation
        {
            get
            {
                return _graphicsElement;
            }
            set
            {
                _graphicsElement = value;
            }
        }

        #endregion IFigureRepresentable

        protected void ShowInfo(object sender, EventArgs e)
        {
            var ent = Entity;
            using (var uow = new UnitOfWorkWithOAM())
            {
                var type = uow.Context.TTypes.FirstOrDefault((e1) => e1.Name == TType.TypeNameStruct);
                var typeTree = TypeVM.CreateTypeInheritanceTree(type);
            }
        }

        protected void AddInputInterface(object sender, EventArgs e)
        {
            var i = new StructuralInputInterface(Owner);
            var repr = GraphicsController.AddInputInterfaceGraphicsRepresentation();
            i.Representation = repr;
            InputInterfaces.Add(i);
        }

        protected void AddOutputInterface(object sender, EventArgs e)
        {
            var i = new StructuralOutputInterface(Owner);
            var repr = GraphicsController.AddOutputInterfaceGraphicsRepresentation();
            i.Representation = repr;
            OutputInterfaces.Add(i);
        }

        protected void ShowInnerDecomposition(object sender, EventArgs e)
        {
            GraphicsController.EditDiagramm(InnerDecomposition);
        }
        
        private ContextMenuStrip _contextMenu;
        public override ContextMenuStrip ContextMenu
        {
            get
            {
                return _contextMenu;
            }
        }

        public override void SaveGraphicalState(UnitOfWorkWithOAM uow)
        {
            var repr = Representation as StructureRepresentation;
            if (repr == null)
                return;

            var ent = TObject.GetObject(uow, Entity.Id);
            if (ent == null)
                return;
            var widthAttr = ent.GetAttributeAsScalarValue(TType.TypeStructAttrNameWidth);
            var heightAttr = ent.GetAttributeAsScalarValue(TType.TypeStructAttrNameHeight);
            var posXAttr = ent.GetAttributeAsScalarValue(TType.TypeStructAttrNameUpperLeftX);
            var posYAttr = ent.GetAttributeAsScalarValue(TType.TypeStructAttrNameUpperLeftY);
            if (widthAttr == null || heightAttr == null || posXAttr == null || posYAttr == null)
            {
                return;
            }
                
            widthAttr.SetValue(repr.Bounds.Width.ToString());
            heightAttr.SetValue(repr.Bounds.Height.ToString());
            posXAttr.SetValue(repr.Bounds.Left.ToString());
            posYAttr.SetValue(repr.Bounds.Top.ToString());

            if (InnerDecomposition == null)
            {
                InnerDecomposition.SaveGraphicalState(uow);
            }
        }

        public override void RestoreGraphicalState(UnitOfWorkWithOAM uow)
        {
            
        }

        #region .ctor

        public Structure(StructuralDiagramm dia)
            :base(dia)
        {
            using (var uow = new UnitOfWorkWithOAM())
            {
                baseElementType = uow.Context.TTypes.FirstOrDefault(t => t.Name == TType.TypeNameStruct);
            }
            _contextMenu = base.ContextMenu;
            _contextMenu.Items.Add(new ToolStripMenuItem("Отобразить информацию", null, ShowInfo));
            _contextMenu.Items.Add(new ToolStripMenuItem("Добавить входной интерфейс", null, AddInputInterface));
            _contextMenu.Items.Add(new ToolStripMenuItem("Добавить выходной интерфейс", null, AddOutputInterface));
            _contextMenu.Items.Add(new ToolStripMenuItem("Определить внутренюю структуру", null, ShowInnerDecomposition));            
        }

        #endregion .ctor
    }
}
