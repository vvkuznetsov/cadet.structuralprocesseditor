﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StructuralProcessEditor.Diagramms;
using StructuralProcessEditor.ProcessDiagramms;
using System.Windows.Forms;
using Diagnostics;
using StructuralProcessEditor.DiagrammExtension;
using ObjectAttributeMapping.OAM.OAMController;

namespace StructuralProcessEditor.StructuralDiagramms
{
    /// <summary>
    /// Структурная диаграмма
    /// </summary>
    public class StructuralDiagramm : LogicalDiagramm
    {
        public StructuralDiagramm UpperLevel { get; set; }
        
        public static IStructuralDiagrammGraphicsController GraphicsController { get; set; }
        
        private List<ProcessDiagramm> _processDiagramms;

        private List<Structure> _structures = new List<Structure>();
        public IEnumerable<Structure> Structures
        {
            get 
            {
                return _structures;
            }
        }

        private List<StructuralConnection> _connections = new List<StructuralConnection>();
        public IEnumerable<StructuralConnection> Connections
        {
            get
            {
                return _connections;
            }
        }

        public void AddConnection(StructuralConnection conn)
        {
            _connections.Add(conn);
        }

        private ContextMenuStrip _contextMenu;
        public override ContextMenuStrip ContextMenu
        {
            get
            {
                return _contextMenu;
            }
        }

        public void AddStructure()
        {
        }

        protected void AddStructureOperation(object sender, EventArgs e)
        {
            Structure s = new Structure(this);
            s.Representation = GraphicsController.AddStructureGraphicsRepresentation();
            if (s.AssignEntity())
            {          
                _structures.Add(s);
            }
            
        }

        protected void ShowUpperLevel(object sender, EventArgs e)
        {
            if (UpperLevel != null)
            {
                GraphicsController.EditDiagramm(UpperLevel);
            }
            else 
            {
                MessageBox.Show("Диаграмма верхнего уровня !");
            }
        }

        public override LogicalDiagrammElement FindElementByGraphicalRepresentation(Figure figure)
        {
            LogicalDiagrammElement e = null;
            foreach (var s in Structures)
            {
                if (s is IElementsContainer)
                {
                    var container = s as IElementsContainer;
                    e = container.FindElementByGraphicalRepresentation(figure);
                    if (e != null)
                    {
                        break;
                    }
                }
                if (s is IFigureRepresentable)
                {
                    if (((IFigureRepresentable)s).Representation == figure)
                    {
                        e = s;
                        break;
                    }
                }
            }
            return e;
        }

        public void SaveGraphicalInfo()
        {
            using (var uow = new UnitOfWorkWithOAM())
            {
                SaveGraphicalState(uow);
            }
        }

        public override void SaveGraphicalState(UnitOfWorkWithOAM uow)
        {
            foreach (var s in Structures)
            {
                if (s is IGraphicalStateable)
                {
                    ((IGraphicalStateable)s).SaveGraphicalState(uow);
                }
            }
            foreach (var c in Connections)
            {
                if (c is IGraphicalStateable)
                {
                    ((IGraphicalStateable)c).SaveGraphicalState(uow);
                }
            }
        }

        #region .ctor

        public StructuralDiagramm()
        {
            _contextMenu = new ContextMenuStrip();
            _contextMenu.Items.Add(new ToolStripMenuItem("Добавить структуру", null, AddStructureOperation));
            _contextMenu.Items.Add(new ToolStripMenuItem("Рассмотреть общую схему", null, ShowUpperLevel));
        }
        
        #endregion .ctor
    }
}
