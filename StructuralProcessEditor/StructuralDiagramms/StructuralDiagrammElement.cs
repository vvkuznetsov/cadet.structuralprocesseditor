﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StructuralProcessEditor.Diagramms;
using ObjectAttributeMapping.OAM.OAMDB;
using System.Windows.Forms;
using StructuralProcessEditor.View;
using StructuralProcessEditor.ViewModel;
using ObjectAttributeMapping.OAM.OAMController;

namespace StructuralProcessEditor.StructuralDiagramms
{
    public class StructuralDiagrammElement : LogicalDiagrammElement
    {
        /// <summary>
        /// Контроллер управления графическим представлением структурных диаграмм
        /// </summary>
        public static IStructuralDiagrammGraphicsController GraphicsController { get; set; }

        /// <summary>
        /// Владелец элемента
        /// </summary>
        public StructuralDiagramm Owner { get; private set; }

        protected TType baseElementType;

        private void ConnectWithObject(object sender, EventArgs e)
        {
            AssignEntity(baseElementType);
        }

        protected void EditAttributeValues(object sender, EventArgs e)
        {
            var objectView = new ObjectView();
            TypeVM typeVM = new TypeVM(Entity.Type);
            var objectVM = new ObjectVM(Entity, typeVM);
            objectVM.UOW = new UnitOfWorkWithOAM(); 
            objectView.DataContext = objectVM;
            objectView.ShowDialog();
            if ((bool)objectView.DialogResult)
            {
                objectVM.UOW.Commit();
            }

        }

        private ContextMenuStrip _contextMenu;
        public override ContextMenuStrip ContextMenu
        {
            get
            {
                return _contextMenu;
            }
        }

        private StructuralDiagrammElement()
        {
            using (var uow = new UnitOfWorkWithOAM())
            {
                baseElementType = uow.Context.TTypes.FirstOrDefault(t => t.Name == TType.TypeNameNamedType);         
            }
            _contextMenu = base.ContextMenu;
            _contextMenu.Items.Add(new ToolStripMenuItem("Связать с объектом", null, ConnectWithObject));
            _contextMenu.Items.Add(new ToolStripMenuItem("Редактировать значения аттрибутов", null, EditAttributeValues));

        }

        public StructuralDiagrammElement(StructuralDiagramm dia) : this()
        {
            Owner = dia;
        }
    }
}
