﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ImageCropControl;

namespace StructuralProcessEditor.View
{
    /// <summary>
    /// Interaction logic for ImageCropperDialog.xaml
    /// </summary>
    public partial class ImageCropperDialog : Window
    {
        public ImageAreaData CroppedImage = null;

        public ImageCropperDialog()
        {
            InitializeComponent();
        }


        private void btnOpenImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFile();
        }



        #region Private Methods
        private void OpenFile()
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
            ofd.Filter = "Images|*.png;*.jpg;*.jpeg;*.bmp;*.gif";
            List<string> allowableFileTypes = new List<string>();
            allowableFileTypes.AddRange(new string[] { ".png", ".jpg", ".jpeg", ".bmp", ".gif" });
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (!ofd.FileName.Equals(String.Empty))
                {
                    FileInfo f = new FileInfo(ofd.FileName);
                    if (allowableFileTypes.Contains(f.Extension.ToLower()))
                    {
                        this.ucImgCrop.ImageUrl = f.FullName;
                    }
                    else
                    {
                        MessageBox.Show("Invalid file type");
                    }
                    btnConfirm.IsEnabled = true;
                }
                else
                {
                    MessageBox.Show("You did pick a file to use");
                }
            }
        }
        #endregion Private Methods

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnConfirm_Click(object sender, RoutedEventArgs e)
        {
            if (ucImgCrop.selectCanvForImg.rubberBand != null)
            {
                var rect = ucImgCrop.selectCanvForImg.rubberBand.RenderedGeometry.GetRenderBounds(new Pen());
                CroppedImage = new ImageAreaData();
                CroppedImage.RectX = (int)rect.X;
                CroppedImage.RectY = (int)rect.Y;
                CroppedImage.RectWidth = (int)(rect.Width / ucImgCrop.zoomFactor);
                CroppedImage.RectHeight = (int)(rect.Height / ucImgCrop.zoomFactor);

                CroppedImage.Fragment = ucImgCrop.bmpSource;
                CroppedImage.ImagePath = ucImgCrop.ImgUrl;
            }
            Close();
        }
    }
}
