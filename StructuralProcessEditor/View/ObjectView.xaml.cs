﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StructuralProcessEditor.View
{
    /// <summary>
    /// Interaction logic for EditObjectView.xaml
    /// </summary>
    public partial class ObjectView : Window
    {
        public ObjectView()
        {
            InitializeComponent();
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
        }

        private void btnAddImage_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new ImageCropperDialog();
            dlg.ShowDialog();
            var pos = new Point(dlg.CroppedImage.RectX, dlg.CroppedImage.RectY);
            // TODO: сохранение изображения с ифной в БД
        }
    }
}
