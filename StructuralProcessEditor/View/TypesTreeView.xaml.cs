﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StructuralProcessEditor.ViewModel;
using System.Collections.ObjectModel;
using StructuralProcessEditor.CommonStructuralProcessController;

namespace StructuralProcessEditor.View
{
    /// <summary>
    /// Interaction logic for TypeView.xaml
    /// </summary>
    public partial class TypesTreeView : UserControl
    {
        public TypesTreeView()
        {
            InitializeComponent();

            StructuralProcessIniter.Initialize();
            DataContext = TypeTreeVM.LoadAllTypes();
        }

        private void treeViewTypes_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var typeTree = DataContext as TypeTreeVM;
            if (typeTree == null)
                return;
            typeTree.SelectedType = e.NewValue as TypeVM;
        }
    }
}
