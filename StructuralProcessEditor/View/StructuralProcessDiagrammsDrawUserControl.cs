﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Diagnostics;
using StructuralProcessEditor.Diagramms;
using StructuralProcessEditor.StructuralDiagramms;
using StructuralProcessEditor.DiagrammExtension;
using System.Windows;

namespace StructuralProcessEditor.View
{
    public partial class StructuralProcessDiagrammsDrawUserControl : DiagramBox, IStructuralDiagrammGraphicsController
    {
        private static ContextMenuStrip BlankContextMenu = new ContextMenuStrip();

        public StructuralProcessDiagrammsDrawUserControl()
        {
            InitializeComponent();

            StructuralDiagrammElement.GraphicsController = this;
            StructuralDiagramm.GraphicsController = this;
        }

        protected LogicalDiagramm _logicalDiagramm;

        public static DependencyProperty LogicDiagrammProperty = DependencyProperty.Register("LogicDiagramm", typeof(LogicalDiagramm), typeof(StructuralProcessDiagrammsDrawUserControl));

        public LogicalDiagramm LogicDiagramm
        {
            get { return _logicalDiagramm; }
            set 
            { 
                _logicalDiagramm = value;
                if (_logicalDiagramm != null)
                {
                    Diagram = _logicalDiagramm.Representation;
                }
            }
        }

        protected LogicalDiagrammElement _selectedDiagrammElement;
        
        protected override ContextMenuStrip MainContextMenu
        {
            get
            {
                if (LogicDiagramm != null)
                    return LogicDiagramm.ContextMenu;
                return BlankContextMenu;
            }
        }

        protected override ContextMenuStrip SelectedElementContextMenu
        {
            get
            {
                if (_selectedDiagrammElement != null)
                    return _selectedDiagrammElement.ContextMenu;
                return BlankContextMenu;
            }
        }

        protected override void Draw(Graphics gr)
        {
            gr.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            if (Diagram != null)
            {

                foreach (Figure f in Diagram.figures)
                    if (!(f is LineFigure))
                        f.Draw(gr);

                foreach (Figure f in Diagram.figures)
                    if (f is LineFigure)
                        f.Draw(gr);
            }

            //рисуем прямоугольник выделенной фигуры
            if (selectedFigure is SolidFigure)
            {
                SolidFigure figure = selectedFigure as SolidFigure;
                RectangleF bounds = figure.Bounds;
                gr.DrawRectangle(selectRectPen, bounds.Left - 2, bounds.Top - 2, bounds.Width + 4, bounds.Height + 4);
            }
            //рисуем маркеры
            foreach (Marker m in markers)
                m.Draw(gr);
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            Focus();
            draggedFigure = FindFigureByPoint(e.Location);


            selectedFigure = draggedFigure;
            if (LogicDiagramm != null)
            {
                _selectedDiagrammElement = LogicDiagramm.FindElementByGraphicalRepresentation(draggedFigure);
            }

            if (!(draggedFigure is Marker))
            {               
                CreateMarkers();
            }

            startDragPoint = e.Location;

            Invalidate();

            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                if (selectedFigure == null)
                    MainContextMenu.Show(PointToScreen(e.Location));
                else
                {
                    if (SelectedElementContextMenu != null)
                    {
                        SelectedElementContextMenu.Show(PointToScreen(e.Location));
                    }
                    else
                    {
                        DefaultSelectedElementContextMenu.Show(PointToScreen(e.Location));
                    }
                }
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            Figure fig = FindFigureByPoint(e.Location);

            if ((!(fig is Marker)) && (fig != null))
            {
                ToolTip1.ToolTipTitle = fig.GetType().ToString();
            }

            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                var draggedFigureTyped = draggedFigure as SolidFigure;
                if (draggedFigureTyped != null)
                {
                    if (draggedFigureTyped.Connectors != null)
                    {
                        foreach (ConnectorFigure con in draggedFigureTyped.Connectors)
                        {
                            con.Offset(e.Location.X - startDragPoint.X, e.Location.Y - startDragPoint.Y);
                        }
                    }
                    draggedFigureTyped.Offset(e.Location.X - startDragPoint.X, e.Location.Y - startDragPoint.Y);
                    UpdateMarkers();
                    Invalidate();
                }
                if (selectedFigure is InterfaceMarker)
                {
                    var marker = selectedFigure as InterfaceMarker;
                    marker.RestrictPossibleLocation();
                }
            }
            else
            {
                Figure figure = FindFigureByPoint(e.Location);
                if (figure is Marker)
                    Cursor = Cursors.SizeAll;
                else
                    if (figure != null)
                        Cursor = Cursors.Hand;
                    else
                        Cursor = Cursors.Default;
            }

            startDragPoint = e.Location;
        }

        #region IStructuralDiagrammGraphicsController

        public SolidFigure AddStructureGraphicsRepresentation()
        {
            if (LogicDiagramm is StructuralDiagramm)
            {
                var fig = AddFigure<StructureRepresentation>(startDragPoint);
                LogicDiagramm.Representation.figures.Add(fig);
                return fig;
            }
            return null;
        }

        public void RemoveStructureGraphicsRepresentation(SolidFigure figure)
        {
            if (LogicDiagramm is StructuralDiagramm)
            {
                Delete(figure);
            }
        }

        public Marker AddInputInterfaceGraphicsRepresentation()
        {
            if (LogicDiagramm is StructuralDiagramm)
            {
                var marker = AddFigure<InputInterfaceMarker>(startDragPoint);
                marker.targetFigure = selectedFigure;
                var gr = selectedFigure as StructureRepresentation;
                if (gr != null)
                {
                    gr.Interfaces.Add(marker);
                }
                return marker;
            }
            return null;
        }

        public Marker AddOutputInterfaceGraphicsRepresentation()
        {
            if (LogicDiagramm is StructuralDiagramm)
            {
                var marker = AddFigure<OutputInterfaceMarker>(startDragPoint);
                marker.targetFigure = selectedFigure;
                var gr = selectedFigure as StructureRepresentation;
                if (gr != null)
                {
                    gr.Interfaces.Add(marker);
                }
                return marker;
            }
            return null;
        }

        public void RemoveInterfaceGraphicsRepresentation(Marker figure)
        {
            throw new NotImplementedException();
        }

        public LineFigure CreateConnectionGraphicsRepresentation(Marker from, Marker to)
        {
            LineFigure connection = null;
            if (LogicDiagramm is StructuralDiagramm)
            {
                connection = new StructuralConnectionRepresentation();
                connection.From = from;
                connection.To = to;
                
                Diagram.figures.Add(connection);
                Invalidate();
            }
            return connection;
        }

        public void EditDiagramm(StructuralDiagramm dia)
        {
            LogicDiagramm = dia;
            Invalidate();
        }

        #endregion IStructuralDiagrammGraphicsController

       
    }
}
