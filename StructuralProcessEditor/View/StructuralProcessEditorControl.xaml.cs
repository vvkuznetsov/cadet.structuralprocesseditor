﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Diagnostics;
using StructuralProcessEditor.Diagramms;

namespace StructuralProcessEditor.View
{
    /// <summary>
    /// Interaction logic for StructuralProcessEditorControl.xaml
    /// </summary>
    public partial class StructuralProcessEditorControl : UserControl
    {
        public StructuralProcessEditorControl()
        {
            InitializeComponent();

            //_spEditor.Diagram = new Diagram();
        }

        private void WindowsFormsHost_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue is  LogicalDiagramm)
            {
                _spEditor.LogicDiagramm = (LogicalDiagramm)e.NewValue;
            }
        }
    }
}
