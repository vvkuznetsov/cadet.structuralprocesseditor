﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ObjectAttributeMapping.OAM.OAMController;
using ObjectAttributeMapping.OAM.OAMDB;

namespace StructuralProcessEditor.CommonStructuralProcessController
{
    /// <summary>
    /// Выполняет первоначальную загрузку
    /// </summary>
    public static class StructuralProcessIniter
    {
        private static ObjectAttributeMappingController _oamController;

        private static bool OAMInited
        {
            get
            {
                try
                {
                    using (var uow = new UnitOfWorkWithOAM())
                    {
                        var c = uow.Context.Configurations.Where<Configuration>((e) => e.Key == TType.ConfigKeyOriginalTypesCreated).FirstOrDefault<Configuration>();
                        if (c == null || !Convert.ToBoolean(c.Value))
                        {
                            return false;
                        }
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            set
            {
                using (var uow = new UnitOfWorkWithOAM())
                {
                    var c = uow.Context.Configurations.Where<Configuration>((e) => e.Key == TType.ConfigKeyOriginalTypesCreated).FirstOrDefault<Configuration>();
                    if (c == null)
                    {
                        c = uow.Context.Configurations.CreateObject();
                        c.Key = TType.ConfigKeyOriginalTypesCreated;
                    }
                    c.Value = Convert.ToString(value);
                    uow.Context.Configurations.AddObject(c);
                }
            }
        }
        
        public static void Initialize(bool? reinit = null)
        {
            if (OAMInited || (reinit.HasValue && !reinit.Value))
                return;

            //Удаляем все типы
            using (var uow = new UnitOfWorkWithOAM())
            {
                foreach (var e in uow.Context.TTypes)
                {
                    uow.Context.TTypes.DeleteObject(e);
                }                
            }
            
            //Создание базовых типов

            #region Тип "Именованный тип"

            var namedTypeAttrs = new List<TAttribute>();
            namedTypeAttrs.Add(new TAttribute(TType.TypeNamedTypeAttrNameName, AttributeType.ScalarValue));
            TType namedType;
            namedType = TType.CreateType(TType.TypeNameNamedType, null, namedTypeAttrs);
            
            var commonParentTypeNames = new string[1]{namedType.Name};

            #endregion Тип "Именованный тип"

            #region Тип "Структура"

            var structAttrs = new List<TAttribute>();

            structAttrs.Add(new TAttribute(TType.TypeStructAttrNameInputInterfaces, AttributeType.VectorReference));
            structAttrs.Add(new TAttribute(TType.TypeStructAttrNameOutputInterfaces, AttributeType.VectorReference));

            structAttrs.Add(new TAttribute(TType.TypeStructAttrNameParentDia, AttributeType.ScalarReference));
            structAttrs.Add(new TAttribute(TType.TypeStructAttrNameInnerDecompositionDia, AttributeType.ScalarReference));

            structAttrs.Add(new TAttribute(TType.TypeStructAttrNameUpperLeftX, AttributeType.ScalarValue));
            structAttrs.Add(new TAttribute(TType.TypeStructAttrNameUpperLeftY, AttributeType.ScalarValue));
            structAttrs.Add(new TAttribute(TType.TypeStructAttrNameWidth, AttributeType.ScalarValue));
            structAttrs.Add(new TAttribute(TType.TypeStructAttrNameHeight, AttributeType.ScalarValue));

            TType.CreateType(TType.TypeNameStruct, commonParentTypeNames, structAttrs);

            #endregion Тип "Структура"

            #region Тип "Интерфейс"

            var ifaceAttrs = new List<TAttribute>();
            ifaceAttrs.Add(new TAttribute(TType.TypeInterfaceAttrNameOwner, AttributeType.ScalarReference));
            ifaceAttrs.Add(new TAttribute(TType.TypeInterfaceAttrNamePositionX, AttributeType.ScalarValue));
            ifaceAttrs.Add(new TAttribute(TType.TypeInterfaceAttrNamePositionY, AttributeType.ScalarValue));
            TType.CreateType(TType.TypeNameInterface, commonParentTypeNames, ifaceAttrs);
            
            #endregion Тип "Интерфейс"

            #region Тип "Соединение"
            
            var connectionAttrs = new List<TAttribute>();
            connectionAttrs.Add(new TAttribute(TType.TypeConnectionAttrNameHeadInterface, AttributeType.ScalarReference));
            connectionAttrs.Add(new TAttribute(TType.TypeConnectionAttrNameTailInterface, AttributeType.ScalarReference));
            TType.CreateType(TType.TypeNameConnection, commonParentTypeNames, connectionAttrs);

            #endregion Тип "Соединение"

            #region Тип "Диаграмма"

            var diaAttrs = new List<TAttribute>();
            diaAttrs.Add(new TAttribute(TType.TypeDiagrammAttrNameElements, AttributeType.VectorReference));

            TType.CreateType(TType.TypeNameDiagramm, commonParentTypeNames, diaAttrs);

            #endregion Тип "Диаграмма"


            #region Тип "Функция"

            var funcAttrs = new List<TAttribute>();
            funcAttrs.Add(new TAttribute(TType.TypeFunctionAttrNameInput, AttributeType.VectorReference));
            funcAttrs.Add(new TAttribute(TType.TypeFunctionAttrNameOutput, AttributeType.VectorReference));
            TType.CreateType(TType.TypeNameFunction, commonParentTypeNames, funcAttrs);

            #endregion Тип "Функция"

            OAMInited = true;
        }

        #region .ctor

        static StructuralProcessIniter()
        {
            _oamController = new ObjectAttributeMappingController();
        }

        #endregion
    }
}
