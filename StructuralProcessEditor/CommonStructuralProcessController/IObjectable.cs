﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ObjectAttributeMapping.OAM.OAMDB;

namespace StructuralProcessEditor.CommonStructuralProcessController
{
    /// <summary>
    /// Интерфейс объекта с которым может быть связан TObject
    /// </summary>
    interface IObjectable
    {
        /// <summary>
        /// Связанная сущность
        /// </summary>
        TObject Entity { get; set; }

        /// <summary>
        /// Связывает с сущностью
        /// </summary>
        bool AssignEntity(TType defType = null);
    }
}
