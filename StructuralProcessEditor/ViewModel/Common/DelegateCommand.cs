﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace StructuralProcessEditor.ViewModel.Common
{
    class DelegateCommand : ICommand
    {
        private Action<object> _execute;
        private Func<object, bool> _canExecute;

        public bool CanExecute(object parameter)
        {
            var e = _canExecute;
            if (e != null)
            {
                return _canExecute(parameter);
            }
            return false;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            var e = _execute;
            if (e != null)
            {
                e(parameter);
            }
        }

        public void OnCanExecuteChanged()
        {
            var e = CanExecuteChanged;
            if (e != null)
            {
                e.Invoke(this, new EventArgs());
            }
        }

        #region .ctor

        public DelegateCommand(Action<object> execute, Func<object, bool> canExecute)
        {
            _execute = execute;
            _canExecute = canExecute;
        }

        #endregion .ctor
    }
}
