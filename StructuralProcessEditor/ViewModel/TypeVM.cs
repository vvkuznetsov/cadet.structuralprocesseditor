﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using StructuralProcessEditor.ViewModel.Common;
using ObjectAttributeMapping.OAM.OAMDB;
using StructuralProcessEditor.View;
using ObjectAttributeMapping.OAM.OAMController;

namespace StructuralProcessEditor.ViewModel
{
    /// <summary>
    /// ViewModel для типа
    /// </summary>
    public class TypeVM : BaseTreeElementVM
    {
        private readonly TType _model;

        #region Свойства

        public int Id
        {
            get 
            {
                return _model.Id;
            }
        }
        
        public virtual string Name
        {
            get 
            {
                return _model.Name;
            }
            set 
            {
                _model.Name = value;
            }
        }

        private TypeVM _baseType;
        public TypeVM BaseType
        {
            get
            {
                return _baseType;
            }
            set
            {
                _baseType = value;
            }
        }

        private ObservableCollection<TypeVM> _inheritedTypes = new ObservableCollection<TypeVM>();
        public ObservableCollection<TypeVM> InheritedTypes
        {
            get
            {
                return _inheritedTypes;
            }
        }

        private ObservableCollection<AttributeVM>  _attributes = new ObservableCollection<AttributeVM>();
        public ObservableCollection<AttributeVM> Attributes
        {
            get 
            {
                return _attributes;
            }
        }

        public void SetAttributes(IEnumerable<AttributeVM> attrs)
        {
            if (attrs != null)
            {
                _attributes = new ObservableCollection<AttributeVM>();
                foreach (var a in attrs)
                    Attributes.Add(a);
            }
        }

        public ContextMenu ContextMenu
        {
            get;
            private set;
        }

        public ContextMenu AttributeMenu
        {
            get;
            private set;
        }

        public static readonly string PropertyNameSelectedAttribute = "SelectedAttribute";
        private AttributeVM _selectedAttribute;
        public AttributeVM SelectedAttribute
        {
            get
            {
                return _selectedAttribute;
            }
            set 
            {
                _selectedAttribute = value;
            }
        }

        #endregion Свойства

        /// <summary>
        /// ИД для типа без родитля
        /// </summary>
        public const int RootTypeId = -1;

        /// <summary>
        /// Выполняет разбиение типов по их роидтелям
        /// </summary>
        /// <param name="types">типы</param>
        /// <returns>словарь: ИД родителя -> список его дочерних типов</returns>
        public static Dictionary<int, List<TypeVM>> OrderTypesByParents(IEnumerable<TType> types) 
        {
            var typesByParent = new Dictionary<int, List<TypeVM>>();

            foreach (var t in types)
            {
                int id;
                if (t.Parent == null)
                    id = RootTypeId;
                else
                    id = t.Parent.Id;
                if (!typesByParent.ContainsKey(id))
                    typesByParent.Add(id, new List<TypeVM>());
                    
                var tvm = new TypeVM(t);
                tvm.SetAttributes(TypeVM.LoadAttributes(tvm, true));
                typesByParent[id].Add(tvm);
            }

            return typesByParent;
        }

        public static TypeVM CreateTypeInheritanceTree(TType root)
        {
            IEnumerable<TType> types = null;
            
            using (var uow = new UnitOfWorkWithOAM())
            {
                types = root.GetInheritedTypes(uow, true);
            }

            if (types == null)
                return null;

            var typesByParent = TypeVM.OrderTypesByParents(types);

            //if (!typesByParent.ContainsKey(RootTypeId) || typesByParent[RootTypeId].Count != 1)
            //    return null;

            var rootVm = typesByParent[RootTypeId].First();

            typesByParent.Remove(RootTypeId);
            ObservableCollection<TypeVM> currentLevel = new ObservableCollection<TypeVM>(new TypeVM[1]{rootVm});
            ObservableCollection<TypeVM> nextLevel = new ObservableCollection<TypeVM>();
            while (typesByParent.Count != 0)
            {
                foreach (var t in currentLevel)
                {
                    if (typesByParent.ContainsKey(t.Id))
                    {
                        foreach (var tvm in typesByParent[t.Id])
                        {
                            t.InheritedTypes.Add(tvm);
                            tvm.BaseType = t;

                            nextLevel.Add(tvm);
                        }
                        typesByParent.Remove(t.Id);
                    }
                }
                currentLevel = nextLevel;
                nextLevel = new ObservableCollection<TypeVM>();
            }

            return rootVm;
        }

        private void AddInheritedTypeCommandExecute(object p)
        {
            using (var uow = new UnitOfWorkWithOAM())
            {
                var typeModel = new TType();

                var typeVM = new TypeVM(typeModel, this);
                
                var addTypeDialog = new AddTypeDialog();
                addTypeDialog.DataContext = typeVM;

                bool? isConfirmed = addTypeDialog.ShowDialog();

                if (isConfirmed.Value)
                {
                    var parent = uow.Context.TTypes.Where<TType>((e) => e.Id == Id).FirstOrDefault<TType>();
                    if (parent != null)
                        typeModel.Parent = parent;
                    uow.Context.TTypes.AddObject(typeModel);
                    InheritedTypes.Add(typeVM);
                    foreach (var a in LoadAttributes(typeVM, uow, true))
                    {
                        typeVM.Attributes.Add(a);
                    }
                }
            }            
        }

        private void RemoveTypeCommandExecute(object p)
        {
            using (var uow = new UnitOfWorkWithOAM())
            {
                var type = uow.Context.TTypes.Where((e) => e.Id == _model.Id).FirstOrDefault();
                if (type != null)
                {
                    BaseType.InheritedTypes.Remove(this);
                    uow.Context.TTypes.DeleteObject(type);
                }
            }
        }

        private void AddAttributeCommandExecute(object p)
        {
            using (var uow = new UnitOfWorkWithOAM())
            {
                var attrM = new TAttribute();
                var attrVM = new AttributeVM(attrM, this);

                var addAttrDialog = new AddAttributeDialog() { DataContext = attrVM };

                bool? isConfirmed = addAttrDialog.ShowDialog();

                if (isConfirmed.Value)
                {
                    attrM.Type = uow.Context.TTypes.Where((e) => e.Id == _model.Id).FirstOrDefault();
                    uow.Context.TAttributes.AddObject(attrM);
                    Attributes.Add(attrVM);
                }
            }
        }

        public static IEnumerable<AttributeVM> LoadAttributes(TypeVM type, bool includeParentAttributes = false)
        {
            using (var uow = new UnitOfWorkWithOAM())
            {
                return LoadAttributes(type, uow, includeParentAttributes);
            }
        }
        
        public static IEnumerable<AttributeVM> LoadAttributes(TypeVM type, UnitOfWorkWithOAM uow, bool includeParentAttributes = false)
        {
            var attrs = new List<AttributeVM>();

            var directAttrsIds = new HashSet<int>(uow.Context.TAttributes.Where((e) => e.Type.Id == type.Id).Select((e) => e.Id));
            foreach (var a in type._model.GetAttributes(uow, includeParentAttributes))
            {
                var isInherited = !directAttrsIds.Contains(a.Id);
                attrs.Add(new AttributeVM(a, type, isInherited));
            }
            
            return attrs;
        }

        public void Test(object p)
        {
            var types = _model.GetAttributes(true);
        }

        #region .ctor

        private TypeVM()
        {
            ContextMenu = new ContextMenu();
            ContextMenu.Items.Add(new MenuItem() { Header = "Добавить наследуемый тип", Command = new DelegateCommand(AddInheritedTypeCommandExecute, (e) => { return true; }) });
            ContextMenu.Items.Add(new MenuItem() { Header = "Удалить тип", Command = new DelegateCommand(RemoveTypeCommandExecute, (e) => { return true; }) });
            ContextMenu.Items.Add(new MenuItem() { Header = "Тест", Command = new DelegateCommand(Test, (e) => { return true; }) });
            
            AttributeMenu = new ContextMenu();
            AttributeMenu.Items.Add(new MenuItem() { Header = "Добавить атрибут", Command = new DelegateCommand(AddAttributeCommandExecute, (e) => { return true; }) });

        }

        public TypeVM(TType type, TypeVM baseType = null) : this()
        {
            if (type == null)
                throw new ArgumentNullException("type");
            _model = type;
            BaseType = baseType;
        }

        #endregion .ctor
    }
}
