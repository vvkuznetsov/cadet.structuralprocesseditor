﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StructuralProcessEditor.ViewModel.Common;
using System.Windows;
using ObjectAttributeMapping.OAM.OAMController;
using ObjectAttributeMapping.OAM.OAMDB;
using System.Data.Objects.DataClasses;
using System.Collections.ObjectModel;
using ObjectAttributeMapping.OAM.OAMDB.TAttributesValues;

namespace StructuralProcessEditor.ViewModel.AttributeValueVMs
{
    public class AttributeValueAdapter : PropertyChangedBase
    {
        #region Data
        TAttrValue _attrValue;
        AttributeType _attrType;
        IEditable<string> _scalarValue;
        IEditable<TObject> _scalarReference;
        IEditableCollection<string, TListValues> _vectorValue;
        IEditableCollection<TObject, TObject> _vectorReference;

        #endregion data 

        #region Public
        public string Name
        {
            get
            {
                return _attrValue.Attribute.Name;
            }
        }

        public string ScalarValue
        {
            get
            {
                if (_scalarValue != null)
                {
                    return _scalarValue.GetValue();
                }
                else
                {
                    return null;
                }
            }
            set
            {
                if (_scalarValue != null)
                {
                    _scalarValue.SetValue(value);
                }
            }
        }

        public TObject ScalarReference
        {
            get
            {
                if (_scalarReference != null)
                {
                    return _scalarReference.GetValue();
                }
                else
                {
                    return null;
                }
            }
            set
            {
                if (_scalarReference != null)
                {
                    _scalarReference.SetValue(value);
                }
            }
        }
        public EntityCollection<TListValues> VectorValue
        {
            get
            {
                if (_vectorValue != null)
                {
                    return _vectorValue.AsEntityCollection();
                }
                else
                {
                    return null;
                }
            }
            set
            {
                if (_vectorValue != null)
                {
                    _vectorValue.SetEntityCollection(value);
                }
                
            }
        }
        public ObservableCollection<TObject> VectorReference
        {
            get
            {
                if (_vectorReference != null)
                {
                    return new ObservableCollection<TObject>(_vectorReference.ToList());
                }
                else 
                {
                    return null;
                }
            }
        }

        public string StringValue
        {
            get
            {
                return this.ToString();
            }
            set
            {
                //nothing
            }
        }
        public override string ToString()
        {
            string res = "";
            switch (Type)
            { 
                case AttributeType.ScalarValue:
                    res = ScalarValue;
                    break;
                case AttributeType.VectorValue:
                    VectorValue.ToList().ForEach(x=> res += x.Value + "; ");
                    break;
                case AttributeType.ScalarReference:
                    //res = _attrValue.AsScalarReference().GetValue().Type.Name;
                    break;
                case AttributeType.VectorReference:
                    //_attrValue.AsVectorReference().ToList().ForEach(x => res += x.Type.Name + "; ");
                    break;        
            }
            if (String.IsNullOrEmpty(res))
            {
                res = "nothing";
            }
            return res;
        }

        public AttributeType Type
        {
            get
            {
                return _attrType;
            }
        }
        #endregion

        #region Service Methods
        static public ObservableCollection<AttributeValueAdapter> GetAttributeValueAdapterCollection(EntityCollection<TAttrValue> attributes)
        {         
            return new ObservableCollection<AttributeValueAdapter>(attributes.Select(a => new AttributeValueAdapter(a)));    
        }
        #endregion

        #region .ctor
        public AttributeValueAdapter(TAttrValue attrValue)
        {
            _attrValue = attrValue;
            _attrType = _attrValue.Attribute.AttrType;
            _scalarValue = attrValue.AsScalarValue();
            _scalarReference = attrValue.AsScalarReference();
            _vectorValue = attrValue.AsVectorValue();
            _vectorReference = attrValue.AsVectorReference();

        }
        #endregion .ctor

    }
}
