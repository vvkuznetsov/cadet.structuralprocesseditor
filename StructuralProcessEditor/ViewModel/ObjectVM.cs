﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StructuralProcessEditor.ViewModel.Common;
using System.Windows.Input;
using ObjectAttributeMapping.OAM.OAMDB;
using StructuralProcessEditor.ViewModel.AttributeValueVMs;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using ObjectAttributeMapping.OAM.OAMController;
using StructuralProcessEditor.View;

namespace StructuralProcessEditor.ViewModel
{
    /// <summary>
    /// ViewModel объекта
    /// </summary>
    class ObjectVM : PropertyChangedBase, IDisposable
    {
        public TObject Entity {get;set;}


        #region Public
        public TypeVM Type 
        { 
            get; 
            private set; 
        }


        private static TypeTreeVM _allTypes;
        public static TypeTreeVM AllTypes
        {
            get
            {
                if (_allTypes == null)
                    _allTypes = TypeTreeVM.LoadAllTypes();

                _allTypes.UpdateTypesInfo();
                return _allTypes;
            }
        }



        private ObservableCollection<AttributeValueAdapter> _attrValues;
        public ObservableCollection<AttributeValueAdapter> AttributesValues
        {
            get 
            {
                if (_attrValues == null)
                {
                    _attrValues = new ObservableCollection<AttributeValueAdapter>();
                }
                return _attrValues;
            }
        }

        protected void TypeAttributesCollectionChangedHandler(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case (NotifyCollectionChangedAction.Add):
                {
                    var newElem = e.NewItems[0] as AttributeVM;
                    break;
                }
                case (NotifyCollectionChangedAction.Remove):
                {
                    break;
                }
            }
        }

        public AttributeValueAdapter SelectedAttribute { get; set; }

        #endregion Public


        #region Commands
        public ICommand EditAttributeCommand { get; private set; }

        protected void EditAttributeCommandExecute(object p)
        {
            //var editAttributeView = new 
            switch (SelectedAttribute.Type)
            { 
                case AttributeType.ScalarValue:
                    var editScalarValueView = new EditScalarValueAttribute();
                    editScalarValueView.DataContext = this;
                    editScalarValueView.ShowDialog();
                    if ((bool)editScalarValueView.DialogResult)
                    { 
                    
                    }
                    break;

                case AttributeType.VectorValue:
                    var editVectorValueView = new EditVectorValueAttribute();
                    editVectorValueView.DataContext = this;
                    editVectorValueView.ShowDialog();
                    if ((bool)editVectorValueView.DialogResult)
                    {

                    }
                    break;

                case AttributeType.ScalarReference:
                    var editScalarReferenceView = new EditScalarReferenceAttribute();
                    editScalarReferenceView.DataContext = this;
                    editScalarReferenceView.ShowDialog();
                    if ((bool)editScalarReferenceView.DialogResult)
                    {
                        
                    }
                    break;
            }

        }

        public ICommand ConfirmCreateObjectCommand { get; private set; }
        
        public ICommand TextSelectionCommand { get; private set; }

        protected void ConfirmCreateObjectCommandExecute(object p)
        {
            Type = AllTypes.SelectedType;
        }

        #endregion Commands

        #region .ctor

        public ObjectVM()
        {
            EditAttributeCommand = new DelegateCommand(EditAttributeCommandExecute, new Func<object, bool>((e) => { return true; }));
            ConfirmCreateObjectCommand = new DelegateCommand(ConfirmCreateObjectCommandExecute, new Func<object, bool>((e) => { return true; }));
            TextSelectionCommand = new DelegateCommand(arg => new TextSelection.View.View.MainWindow().Show(), arg => true);
            UOW = null;
            Objects = null;
        }

        public ObjectVM(TType deftype):this()
        {
            if (deftype != null)
            {
                Type = TypeVM.CreateTypeInheritanceTree(deftype);
            }
        }

        public ObjectVM(TObject obj, TypeVM type)
            : this()
        {
            Entity = obj;
            Type = type;
            _attrValues = AttributeValueAdapter.GetAttributeValueAdapterCollection(Entity.Attributes);
            type.Attributes.CollectionChanged += TypeAttributesCollectionChangedHandler;
        }

        #endregion .ctor

        #region IDisposable

        public void Dispose()
        {
            Type.Attributes.CollectionChanged -= TypeAttributesCollectionChangedHandler;
        }

        #endregion IDisposable

        public IEnumerable<object> Objects { get; set; }
        public UnitOfWorkWithOAM UOW 
        {
            get
            {
                return uow;
            }
            set
            {
                uow = value;
                if (uow != null)
                {
                    Objects = uow.Context.TObjects
                        .ToList()
                        .Select(
                        x => 
                            {
                                x.UoW = uow;
                                return new { Object = x, Name = x.GetAttributeAsScalarValue(TType.TypeNamedTypeAttrNameName).GetValue() };
                            });
                }
            }
        }
        private UnitOfWorkWithOAM uow;
    }
}
