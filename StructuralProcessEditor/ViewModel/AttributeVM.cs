﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using ObjectAttributeMapping.OAM.OAMDB;
using ObjectAttributeMapping.OAM.OAMController;
using System.Windows.Controls;
using StructuralProcessEditor.ViewModel.Common;
using StructuralProcessEditor.View;
using StructuralProcessEditor.ViewModel.AttributeValueVMs;

namespace StructuralProcessEditor.ViewModel
{
    /// <summary>
    /// ViewModel для атрибута типа
    /// </summary>
    public class AttributeVM : PropertyChangedBase
    {
        protected readonly TAttribute _model;

        #region Свойства

        public int Id
        {
            get 
            {
                return _model.Id;
            }
        }
        
        public TypeVM Type
        {
            get;
            set;
        }
        
        public string Name
        {
            get { return _model.Name; }
            set
            {
                _model.Name = value;
            }
        }

        public AttributeType AttributeType
        {
            get { return _model.AttrType; }
            set 
            {
                _model.AttrType = value;
            }
        }

        public AttributeValueAdapter Value
        {
            get;
            set;
        }

        private static Dictionary<AttributeType, string> _attributesTypes = new Dictionary<AttributeType,string>()
        {
            {AttributeType.ScalarReference, "Ссылка"},
            {AttributeType.ScalarValue, "Значение"},
            {AttributeType.VectorReference, "Массив ссылок"},
            {AttributeType.VectorValue, "Массив значений"}
        };

        public Dictionary<AttributeType, string> AttributesTypes
        {
            get 
            {
                return _attributesTypes;
            }   
        }

        public ContextMenu Menu
        {
            get;
            private set;
        }

        #endregion Свойства

        private void RemoveAttributeCommandExecute(object p)
        {
            using (var uow = new UnitOfWorkWithOAM())
            {
                var attr = uow.Context.TAttributes.Where((e) => e.Id == Id).FirstOrDefault();

                uow.Context.TAttributes.DeleteObject(attr);
                Type.Attributes.Remove(this);
            }
        }

        public bool IsInherited
        {
            get;
            private set;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propName)
        {
            var h = PropertyChanged;
            if (h != null)
            {
                h.Invoke(this, new PropertyChangedEventArgs(propName));
            }
        }

        #region .ctor

        public AttributeVM()
        {
            Menu = new ContextMenu();
            Menu.Items.Add(new MenuItem() { Header = "Удалить", Command = new DelegateCommand(RemoveAttributeCommandExecute, (e) => { return true; }) });
        }
        
        public AttributeVM(TAttribute attr, TypeVM type, bool isInherited = false) : this()
        {
            if (attr == null)
                throw new ArgumentNullException("attr");
            _model = attr;
            Type = type;
            IsInherited = isInherited;
        }

        #endregion

        public string Representation
        {
            get 
            {
                return string.Format("[{0}] {1}", AttributesTypes[AttributeType], Name);
            }
        }
    }
}
