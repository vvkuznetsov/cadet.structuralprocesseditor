﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using ObjectAttributeMapping.OAM.OAMController;
using ObjectAttributeMapping.OAM.OAMDB;

namespace StructuralProcessEditor.ViewModel
{
    /// <summary>
    /// ViewModel для дерева типов
    /// </summary>
    public class TypeTreeVM : BaseTreeElementVM
    {
        private ObservableCollection<TypeVM> _types = new ObservableCollection<TypeVM>();
        public ObservableCollection<TypeVM> Types
        {
            get
            {
                return _types;
            }
        }

        private TypeVM _selectedType;
        public TypeVM SelectedType
        {
            get 
            {
                return _selectedType;
            }
            set 
            {
                _selectedType = value;
                RaisePropertyChanged("SelectedType");
            }
        }

        public void UpdateTypesInfo()
        {
            var typesById = new Dictionary<int, TypeVM>();
            var handledParents = new HashSet<int>();

            Types.Clear();

            const int RootTypeId = -1;
            using (var uow = new UnitOfWorkWithOAM())
            {
                var types = uow.Context.TTypes;
                var typesByParent = TypeVM.OrderTypesByParents(types);

                //Добавляем все типы у которых нет родителей
                if (typesByParent.ContainsKey(RootTypeId))
                {
                    foreach (var t in typesByParent[RootTypeId])
                    {
                        Types.Add(t);
                    }
                }

                typesByParent.Remove(RootTypeId);
                ObservableCollection<TypeVM> currentLevel = Types;
                ObservableCollection<TypeVM> nextLevel = new ObservableCollection<TypeVM>();
                while (typesByParent.Count != 0)
                {
                    foreach (var t in currentLevel)
                    {
                        if (typesByParent.ContainsKey(t.Id))
                        {
                            foreach (var tvm in typesByParent[t.Id])
                            {
                                t.InheritedTypes.Add(tvm);
                                tvm.BaseType = t;

                                nextLevel.Add(tvm);
                            }
                            typesByParent.Remove(t.Id);
                        }
                    }
                    currentLevel = nextLevel;
                    nextLevel = new ObservableCollection<TypeVM>();
                }
            }
        }

        public static TypeTreeVM LoadAllTypes()
        {
            var tt = new TypeTreeVM();

            tt.UpdateTypesInfo();
            
            return tt;
        }
    }
}
