﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StructuralProcessEditor.ViewModel.Common;
using System.Windows.Input;
using StructuralProcessEditor.Diagramms;
using StructuralProcessEditor.StructuralDiagramms;
using ObjectAttributeMapping.OAM.OAMController;

namespace StructuralProcessEditor.ViewModel
{
    class StructuralProcessEditorVM : PropertyChangedBase
    {
        /// <summary>
        /// Дерево типов
        /// </summary>
        public TypeTreeVM Types
        {
            get
            {
                return TypeTreeVM.LoadAllTypes();
            }                
        }

        /// <summary>
        /// Текущая редактируемая диаграмма
        /// </summary>
        public LogicalDiagramm CurrentDiagramm
        { 
            get; 
            set; 
        }

        protected void SaveGraphicalRepresentationCommandExecute(object p)
        {
            using (var uow = new UnitOfWorkWithOAM())
            {
                CurrentDiagramm.SaveGraphicalState(uow);
            }
        }

        protected void CreateNewDiagrammCommandExecute(object p)
        {
            CurrentDiagramm = new StructuralDiagramm();
        }

        public ICommand SaveGraphicalRepresentationCommand
        {
            get
            {
                return new DelegateCommand(SaveGraphicalRepresentationCommandExecute, (e) => true);
            }
        }

        public ICommand CreateNewDiagrammCommand
        {
            get 
            {
                return new DelegateCommand(CreateNewDiagrammCommandExecute, (e) => true);
            }
        }

        public StructuralProcessEditorVM()
        {
            CurrentDiagramm = new StructuralDiagramm();
        }
    }
}
