
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 07/27/2013 17:54:11
-- Generated from EDMX file: C:\SPE\ObjectAttributeMapping\OAM\OAMDB\OAM.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [StructProcessEditor];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_ObjectsTypes]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TObjects] DROP CONSTRAINT [FK_ObjectsTypes];
GO
IF OBJECT_ID(N'[dbo].[FK_TypesAttributes]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TAttributes] DROP CONSTRAINT [FK_TypesAttributes];
GO
IF OBJECT_ID(N'[dbo].[FK_TTypeTType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TTypes] DROP CONSTRAINT [FK_TTypeTType];
GO
IF OBJECT_ID(N'[dbo].[FK_TAttributeTBaseAttrValue]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TAttrValues] DROP CONSTRAINT [FK_TAttributeTBaseAttrValue];
GO
IF OBJECT_ID(N'[dbo].[FK_TObjectTAttrValue]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TAttrValues] DROP CONSTRAINT [FK_TObjectTAttrValue];
GO
IF OBJECT_ID(N'[dbo].[FK_TReferenceScalarAttrValueTObject]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TObjects] DROP CONSTRAINT [FK_TReferenceScalarAttrValueTObject];
GO
IF OBJECT_ID(N'[dbo].[FK_TVectorAttrValueTListValues]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TListValues] DROP CONSTRAINT [FK_TVectorAttrValueTListValues];
GO
IF OBJECT_ID(N'[dbo].[FK_TObjectTKeyValueData]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TKeyValueDatas] DROP CONSTRAINT [FK_TObjectTKeyValueData];
GO
IF OBJECT_ID(N'[dbo].[FK_TReferenceVectorAttrValueTObject]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TObjects] DROP CONSTRAINT [FK_TReferenceVectorAttrValueTObject];
GO
IF OBJECT_ID(N'[dbo].[FK_TReferenceScalarAttrValue_inherits_TAttrValue]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TAttrValues_TReferenceScalarAttrValue] DROP CONSTRAINT [FK_TReferenceScalarAttrValue_inherits_TAttrValue];
GO
IF OBJECT_ID(N'[dbo].[FK_TVectorAttrValue_inherits_TAttrValue]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TAttrValues_TVectorAttrValue] DROP CONSTRAINT [FK_TVectorAttrValue_inherits_TAttrValue];
GO
IF OBJECT_ID(N'[dbo].[FK_TReferenceVectorAttrValue_inherits_TAttrValue]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TAttrValues_TReferenceVectorAttrValue] DROP CONSTRAINT [FK_TReferenceVectorAttrValue_inherits_TAttrValue];
GO
IF OBJECT_ID(N'[dbo].[FK_TScalarAttrValue_inherits_TAttrValue]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TAttrValues_TScalarAttrValue] DROP CONSTRAINT [FK_TScalarAttrValue_inherits_TAttrValue];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[TObjects]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TObjects];
GO
IF OBJECT_ID(N'[dbo].[TTypes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TTypes];
GO
IF OBJECT_ID(N'[dbo].[TAttributes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TAttributes];
GO
IF OBJECT_ID(N'[dbo].[TAttrValues]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TAttrValues];
GO
IF OBJECT_ID(N'[dbo].[TListValues]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TListValues];
GO
IF OBJECT_ID(N'[dbo].[TKeyValueDatas]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TKeyValueDatas];
GO
IF OBJECT_ID(N'[dbo].[Configurations]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Configurations];
GO
IF OBJECT_ID(N'[dbo].[TAttrValues_TReferenceScalarAttrValue]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TAttrValues_TReferenceScalarAttrValue];
GO
IF OBJECT_ID(N'[dbo].[TAttrValues_TVectorAttrValue]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TAttrValues_TVectorAttrValue];
GO
IF OBJECT_ID(N'[dbo].[TAttrValues_TReferenceVectorAttrValue]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TAttrValues_TReferenceVectorAttrValue];
GO
IF OBJECT_ID(N'[dbo].[TAttrValues_TScalarAttrValue]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TAttrValues_TScalarAttrValue];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'TObjects'
CREATE TABLE [dbo].[TObjects] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TReferenceVectorAttrValueId] int  NULL,
    [Type_Id] int  NOT NULL,
    [TReferenceScalarAttrValueTObject_TObject_Id] int  NULL
);
GO

-- Creating table 'TTypes'
CREATE TABLE [dbo].[TTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Parent_Id] int  NULL
);
GO

-- Creating table 'TAttributes'
CREATE TABLE [dbo].[TAttributes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [AttrTypeNumber] tinyint  NOT NULL,
    [Info] nvarchar(max)  NULL,
    [Type_Id] int  NOT NULL
);
GO

-- Creating table 'TAttrValues'
CREATE TABLE [dbo].[TAttrValues] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Attribute_Id] int  NULL,
    [Object_Id] int  NOT NULL
);
GO

-- Creating table 'TListValues'
CREATE TABLE [dbo].[TListValues] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Value] nvarchar(max)  NOT NULL,
    [TVectorAttrValueId] int  NOT NULL
);
GO

-- Creating table 'TKeyValueDatas'
CREATE TABLE [dbo].[TKeyValueDatas] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Value] nvarchar(max)  NOT NULL,
    [TObjectId] int  NOT NULL
);
GO

-- Creating table 'Configurations'
CREATE TABLE [dbo].[Configurations] (
    [Key] nvarchar(max)  NOT NULL,
    [Value] nvarchar(max)  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'TAttrValues_TReferenceScalarAttrValue'
CREATE TABLE [dbo].[TAttrValues_TReferenceScalarAttrValue] (
    [Id] int  NOT NULL
);
GO

-- Creating table 'TAttrValues_TVectorAttrValue'
CREATE TABLE [dbo].[TAttrValues_TVectorAttrValue] (
    [Id] int  NOT NULL
);
GO

-- Creating table 'TAttrValues_TReferenceVectorAttrValue'
CREATE TABLE [dbo].[TAttrValues_TReferenceVectorAttrValue] (
    [TObjectId] int  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'TAttrValues_TScalarAttrValue'
CREATE TABLE [dbo].[TAttrValues_TScalarAttrValue] (
    [Value] nvarchar(max)  NULL,
    [Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'TObjects'
ALTER TABLE [dbo].[TObjects]
ADD CONSTRAINT [PK_TObjects]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TTypes'
ALTER TABLE [dbo].[TTypes]
ADD CONSTRAINT [PK_TTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TAttributes'
ALTER TABLE [dbo].[TAttributes]
ADD CONSTRAINT [PK_TAttributes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TAttrValues'
ALTER TABLE [dbo].[TAttrValues]
ADD CONSTRAINT [PK_TAttrValues]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TListValues'
ALTER TABLE [dbo].[TListValues]
ADD CONSTRAINT [PK_TListValues]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TKeyValueDatas'
ALTER TABLE [dbo].[TKeyValueDatas]
ADD CONSTRAINT [PK_TKeyValueDatas]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Configurations'
ALTER TABLE [dbo].[Configurations]
ADD CONSTRAINT [PK_Configurations]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TAttrValues_TReferenceScalarAttrValue'
ALTER TABLE [dbo].[TAttrValues_TReferenceScalarAttrValue]
ADD CONSTRAINT [PK_TAttrValues_TReferenceScalarAttrValue]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TAttrValues_TVectorAttrValue'
ALTER TABLE [dbo].[TAttrValues_TVectorAttrValue]
ADD CONSTRAINT [PK_TAttrValues_TVectorAttrValue]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TAttrValues_TReferenceVectorAttrValue'
ALTER TABLE [dbo].[TAttrValues_TReferenceVectorAttrValue]
ADD CONSTRAINT [PK_TAttrValues_TReferenceVectorAttrValue]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TAttrValues_TScalarAttrValue'
ALTER TABLE [dbo].[TAttrValues_TScalarAttrValue]
ADD CONSTRAINT [PK_TAttrValues_TScalarAttrValue]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Type_Id] in table 'TObjects'
ALTER TABLE [dbo].[TObjects]
ADD CONSTRAINT [FK_ObjectsTypes]
    FOREIGN KEY ([Type_Id])
    REFERENCES [dbo].[TTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ObjectsTypes'
CREATE INDEX [IX_FK_ObjectsTypes]
ON [dbo].[TObjects]
    ([Type_Id]);
GO

-- Creating foreign key on [Type_Id] in table 'TAttributes'
ALTER TABLE [dbo].[TAttributes]
ADD CONSTRAINT [FK_TypesAttributes]
    FOREIGN KEY ([Type_Id])
    REFERENCES [dbo].[TTypes]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TypesAttributes'
CREATE INDEX [IX_FK_TypesAttributes]
ON [dbo].[TAttributes]
    ([Type_Id]);
GO

-- Creating foreign key on [Parent_Id] in table 'TTypes'
ALTER TABLE [dbo].[TTypes]
ADD CONSTRAINT [FK_TTypeTType]
    FOREIGN KEY ([Parent_Id])
    REFERENCES [dbo].[TTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TTypeTType'
CREATE INDEX [IX_FK_TTypeTType]
ON [dbo].[TTypes]
    ([Parent_Id]);
GO

-- Creating foreign key on [Attribute_Id] in table 'TAttrValues'
ALTER TABLE [dbo].[TAttrValues]
ADD CONSTRAINT [FK_TAttributeTBaseAttrValue]
    FOREIGN KEY ([Attribute_Id])
    REFERENCES [dbo].[TAttributes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TAttributeTBaseAttrValue'
CREATE INDEX [IX_FK_TAttributeTBaseAttrValue]
ON [dbo].[TAttrValues]
    ([Attribute_Id]);
GO

-- Creating foreign key on [Object_Id] in table 'TAttrValues'
ALTER TABLE [dbo].[TAttrValues]
ADD CONSTRAINT [FK_TObjectTAttrValue]
    FOREIGN KEY ([Object_Id])
    REFERENCES [dbo].[TObjects]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TObjectTAttrValue'
CREATE INDEX [IX_FK_TObjectTAttrValue]
ON [dbo].[TAttrValues]
    ([Object_Id]);
GO

-- Creating foreign key on [TReferenceScalarAttrValueTObject_TObject_Id] in table 'TObjects'
ALTER TABLE [dbo].[TObjects]
ADD CONSTRAINT [FK_TReferenceScalarAttrValueTObject]
    FOREIGN KEY ([TReferenceScalarAttrValueTObject_TObject_Id])
    REFERENCES [dbo].[TAttrValues_TReferenceScalarAttrValue]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TReferenceScalarAttrValueTObject'
CREATE INDEX [IX_FK_TReferenceScalarAttrValueTObject]
ON [dbo].[TObjects]
    ([TReferenceScalarAttrValueTObject_TObject_Id]);
GO

-- Creating foreign key on [TVectorAttrValueId] in table 'TListValues'
ALTER TABLE [dbo].[TListValues]
ADD CONSTRAINT [FK_TVectorAttrValueTListValues]
    FOREIGN KEY ([TVectorAttrValueId])
    REFERENCES [dbo].[TAttrValues_TVectorAttrValue]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TVectorAttrValueTListValues'
CREATE INDEX [IX_FK_TVectorAttrValueTListValues]
ON [dbo].[TListValues]
    ([TVectorAttrValueId]);
GO

-- Creating foreign key on [TObjectId] in table 'TKeyValueDatas'
ALTER TABLE [dbo].[TKeyValueDatas]
ADD CONSTRAINT [FK_TObjectTKeyValueData]
    FOREIGN KEY ([TObjectId])
    REFERENCES [dbo].[TObjects]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TObjectTKeyValueData'
CREATE INDEX [IX_FK_TObjectTKeyValueData]
ON [dbo].[TKeyValueDatas]
    ([TObjectId]);
GO

-- Creating foreign key on [TReferenceVectorAttrValueId] in table 'TObjects'
ALTER TABLE [dbo].[TObjects]
ADD CONSTRAINT [FK_TReferenceVectorAttrValueTObject]
    FOREIGN KEY ([TReferenceVectorAttrValueId])
    REFERENCES [dbo].[TAttrValues_TReferenceVectorAttrValue]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_TReferenceVectorAttrValueTObject'
CREATE INDEX [IX_FK_TReferenceVectorAttrValueTObject]
ON [dbo].[TObjects]
    ([TReferenceVectorAttrValueId]);
GO

-- Creating foreign key on [Id] in table 'TAttrValues_TReferenceScalarAttrValue'
ALTER TABLE [dbo].[TAttrValues_TReferenceScalarAttrValue]
ADD CONSTRAINT [FK_TReferenceScalarAttrValue_inherits_TAttrValue]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[TAttrValues]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'TAttrValues_TVectorAttrValue'
ALTER TABLE [dbo].[TAttrValues_TVectorAttrValue]
ADD CONSTRAINT [FK_TVectorAttrValue_inherits_TAttrValue]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[TAttrValues]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'TAttrValues_TReferenceVectorAttrValue'
ALTER TABLE [dbo].[TAttrValues_TReferenceVectorAttrValue]
ADD CONSTRAINT [FK_TReferenceVectorAttrValue_inherits_TAttrValue]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[TAttrValues]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'TAttrValues_TScalarAttrValue'
ALTER TABLE [dbo].[TAttrValues_TScalarAttrValue]
ADD CONSTRAINT [FK_TScalarAttrValue_inherits_TAttrValue]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[TAttrValues]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------