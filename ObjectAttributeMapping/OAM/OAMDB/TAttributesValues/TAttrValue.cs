﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ObjectAttributeMapping.OAM.OAMController;
using ObjectAttributeMapping.OAM.OAMDB.TAttributesValues;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace ObjectAttributeMapping.OAM.OAMDB
{
    /// <summary>
    /// Базовый класс значения аттрибута
    /// </summary>
    public partial class TAttrValue
    {
        //protected static BinaryFormatter _serializer = new BinaryFormatter();

        ///// <summary>
        ///// Устанавливает себя, в качестве значения аттрибута для объекта
        ///// </summary>
        ///// <param name="obj">объект</param>
        ///// <param name="attribute">аттрибут</param>
        ///// <param name="uow">единица работы</param>
        ///// <remarks>переопределить в наследниках для изменения поведения</remarks>
        //public virtual void SetAttributeValue(TObject obj, TAttribute attribute, UnitOfWorkWithOAM uow) { }

        //public virtual T GetAttributeValue<T>(TObject obj, TAttribute attribute, UnitOfWorkWithOAM uow) where T : class
        //{
        //    return null;
        //}

        ///// <summary>
        ///// Возвуращает обобщенное значение атрибута объекта
        ///// </summary>
        ///// <param name="obj">объект</param>
        ///// <param name="attribute">атрибут</param>
        ///// <param name="uow">единица работы OAM</param>
        ///// <returns>значение атрибута</returns>
        //protected TAttrValue GetObjectAttributeValue(TObject obj, TAttribute attribute, UnitOfWorkWithOAM uow)
        //{
        //    return uow.Context.TAttrValues.Where<TAttrValue>((e, i) => e.Object.Id == obj.Id && e.Attribute.Name == attribute.Name).FirstOrDefault<TAttrValue>();
        //}

        public static TAttrValue CreateInstance(AttributeType type)
        {
            switch (type)
            {
                case AttributeType.ScalarReference:
                    {
                        return new TReferenceScalarAttrValue();
                    }
                case AttributeType.ScalarValue:
                    {
                        return new TScalarAttrValue();
                    }
                case AttributeType.VectorReference:
                    {
                        return new TReferenceVectorAttrValue();
                    }
                case AttributeType.VectorValue:
                    {
                        return new TVectorAttrValue();
                    }
            }
            return null;
        }


        #region Интерфейсы доступа к значениям

        public virtual IEditableCollection<TObject, TObject> AsVectorReference()
        {
            return null;
        }

        public virtual IEditableCollection<string, TListValues> AsVectorValue()
        {
            return null;
        }

        public virtual IEditable<TObject> AsScalarReference()
        {
            return null;
        }

        public virtual IEditable<string> AsScalarValue()
        {
            return null;
        }

        #endregion Интерфейсы доступа к значениям

        //protected T Deserialize<T>(byte[] data)
        //{
        //    using (var stream = new MemoryStream(data))
        //    {
        //        var o = _serializer.Deserialize(stream);
        //        T typed = default(T);
        //        try
        //        {
        //            typed = (T)o;
        //        }
        //        catch (Exception ex) 
        //        { }
        //        return typed;
        //    }
        //}

        //protected byte[] Serialize<T>(T item)
        //{
        //    var memStream = new MemoryStream();
        //    _serializer.Serialize(memStream, item);
        //    return memStream.ToArray();
        //}
    }
}
