﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectAttributeMapping.OAM.OAMDB.TAttributesValues
{
    public interface IEditable<T>
    {
        T GetValue();
        void SetValue(T item);
    }
}
