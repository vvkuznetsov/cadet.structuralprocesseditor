﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ObjectAttributeMapping.OAM.OAMDB.TAttributesValues;
using System.ComponentModel;
using System.Data.Objects.DataClasses;

namespace ObjectAttributeMapping.OAM.OAMDB
{
    public partial class TVectorAttrValue : IEditableCollection<string, TListValues>
    {
        public override IEditableCollection<string, TListValues> AsVectorValue()
        {
            return (IEditableCollection<string, TListValues>)this;
        }

        #region IEditableCollection<TObject, TListValues>

        public void Add(string item)
        {
            Values.Add(new TListValues() { Value = item });
        }

        public void Remove(string item)
        {
            var data = Values.FirstOrDefault((e) => e.Value == item);
            if (data != null)
                Values.Remove(data);
        }

        public List<string> ToList()
        {
            return Values.Select(x=>x.Value).ToList();
        }

        public EntityCollection<TListValues> AsEntityCollection()
        {
            return Values;
        }

        public void SetEntityCollection(EntityCollection<TListValues> values)
        {
            Values = values;
        }

        #endregion IEditableCollection<TObject, TListValues>





    }
}
