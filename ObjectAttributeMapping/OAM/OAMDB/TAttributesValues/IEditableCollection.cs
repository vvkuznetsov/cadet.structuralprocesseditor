﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects.DataClasses;

namespace ObjectAttributeMapping.OAM.OAMDB.TAttributesValues
{
    public interface IEditableCollection<T, TValueItem> where TValueItem : class
    {
        void Add(T item);
        void Remove(T item);
        List<T> ToList();
        EntityCollection<TValueItem> AsEntityCollection();
        void SetEntityCollection(EntityCollection<TValueItem> values);
    }
}
