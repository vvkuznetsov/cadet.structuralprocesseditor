﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ObjectAttributeMapping.OAM.OAMDB.TAttributesValues;

namespace ObjectAttributeMapping.OAM.OAMDB
{
    public partial class TReferenceScalarAttrValue : IEditable<TObject>
    {
        public override IEditable<TObject> AsScalarReference()
        {
            return (IEditable<TObject>)this;
        }
        
        #region IEditable<TObject>

        public TObject GetValue()
        {
            return Value;
        }

        public void SetValue(TObject item)
        {
            Value = item;
        }

        #endregion IEditable<TObject>
    }
}
