﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ObjectAttributeMapping.OAM.OAMDB.TAttributesValues;
using System.Data.Objects.DataClasses;

namespace ObjectAttributeMapping.OAM.OAMDB
{
    public partial class TReferenceVectorAttrValue : IEditableCollection<TObject, TObject>
    {
        public override IEditableCollection<TObject, TObject> AsVectorReference()
        {
            return (IEditableCollection<TObject, TObject>)this;
        }

        #region IEditableCollection<TObject, TObject>

        public void Add(TObject item)
        {
            Values.Add(item);
        }

        public void Remove(TObject item)
        {
            Values.Remove(item);
        }

        public List<TObject> ToList()
        {
            return Values.ToList();
        }

        public EntityCollection<TObject> AsEntityCollection()
        {
            return Values;
        }

        public void SetEntityCollection(EntityCollection<TObject> values)
        {
            Values = values;
        }

        #endregion IEditableCollection<TObject, TObject>



    }
}
