﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ObjectAttributeMapping.OAM.OAMController;
using ObjectAttributeMapping.OAM.OAMDB.TAttributesValues;
using System.Collections.Specialized;

namespace ObjectAttributeMapping.OAM.OAMDB
{
    /// <summary>
    /// Значение скалядрного атрибута
    /// </summary>
    public partial class TScalarAttrValue : IEditable<string>
    {
        public override IEditable<string> AsScalarValue()
        {
 	         return (IEditable<string>)this;
        }

        #region IEditable

        public string GetValue()
        {
            return Value;
        }

        public void SetValue(string item)
        {
            Value = item;
        }

        #endregion IEditable       
    }
}