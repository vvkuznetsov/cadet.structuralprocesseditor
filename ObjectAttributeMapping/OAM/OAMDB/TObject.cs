﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ObjectAttributeMapping.OAM.OAMController;
using ObjectAttributeMapping.OAM.OAMDB.TAttributesValues;

namespace ObjectAttributeMapping.OAM.OAMDB
{
    /// <summary>
    /// Объект OAM
    /// </summary>
    public partial class TObject : IUoWConnectable
    {
        ///// <summary>
        ///// Установить значение атрибута объекта
        ///// </summary>
        ///// <param name="attribute">атрибут</param>
        ///// <param name="value">значение</param>
        //public void SetAttributeValue(TAttribute attribute, TAttrValue value)
        //{
        //    if (attribute == null || value == null)
        //        return;
            
        //    using (var uow = new UnitOfWorkWithOAM())
        //    {
        //        var attr = Type.Attributes.Contains(attribute);
        //        if (attr == null)
        //            throw new ArgumentException(string.Format("Ошибка: тип {0} не содержит аттрибут {1}", Type.Name, attribute.Name));

        //        uow.Context.TAttrValues.AddObject(value);
        //    }
        //}

        ///// <summary>
        ///// Установить значение атрибута объекта в рамках заданной единицы работы
        ///// </summary>
        ///// <param name="attribute">атрибут</param>
        ///// <param name="value">значение</param>
        ///// <param name="uow">единица работы</param>
        //public void SetAttributeValue(TAttribute attribute, TAttrValue value, UnitOfWorkWithOAM uow)
        //{
 
        //}

        public static TObject GetObject(UnitOfWorkWithOAM uow, int id)
        {
            return uow.Context.TObjects.FirstOrDefault((e) => e.Id == id);
        }

        private TAttrValue GetAttribute(string attrName)
        {
            return Attributes.FirstOrDefault((e) => e.Attribute.Name == attrName);
        }

        public IEditable<TObject> GetAttributeAsScalareRefernce(string attrName)
        {
            var attr = GetAttribute(attrName);
            return (attr == null) ? null : attr.AsScalarReference();
        }

        public IEditable<string> GetAttributeAsScalarValue(string attrName)
        {
            var attr = GetAttribute(attrName);
            return (attr == null) ? null : attr.AsScalarValue();
        }

        public IEditableCollection<TObject, TObject> GetAttributeAsVectorReference(string attrName)
        {
            var attr = GetAttribute(attrName);
            return (attr == null) ? null : attr.AsVectorReference();
        }

        public IEditableCollection<string, TListValues> GetAttributeAsVectorValue(string attrName)
        {
            var attr = GetAttribute(attrName);
            return (attr == null) ? null : attr.AsVectorValue();
        }
        
        ///// <summary>
        ///// Возвращает значение атрибута объекта
        ///// </summary>
        ///// <param name="attribute">атрибут</param>
        ///// <returns>значение атрибута</returns>
        //public TAttrValue GetAttributeValue(TAttribute attribute)
        //{
        //    if (attribute == null)
        //        return null;

        //    TAttrValue value = null;
        //    using (var uow = new UnitOfWorkWithOAM())
        //    {
        //        var attr = Type.Attributes.Contains(attribute);
        //        if (attr == null)
        //            throw new ArgumentException(string.Format("Ошибка: тип {0} не содержит аттрибут {1}", Type.Name, attribute.Name));

        //        value = uow.Context.TAttrValues.Where<TAttrValue>((e, i) => e.Object.Id == Id && e.Attribute.Name == attribute.Name).FirstOrDefault<TAttrValue>();
        //    }
        //    return value;
        //}

        //public T GetAttributeValue<T>(TAttribute attribute) where T : class
        //{
        //    if (attribute == null)
        //        return null;
            
        //    T value = null;
        //    using (var uow = new UnitOfWorkWithOAM())
        //    {
        //        var attr = Type.Attributes.Contains(attribute);
        //        if (attr == null)
        //            throw new ArgumentException(string.Format("Ошибка: тип {0} не содержит аттрибут {1}", Type.Name, attribute.Name));

        //        //uow.Context.TAttrValues.Where<TAttrValue>((e, i) => e.Object.Id == Id && e.Attribute.Name == attrName).FirstOrDefault<TAttrValue>() as TScalarAttrValue;
        //    }
        //    return value;
        //}

        

        ////public string GetScalarAttribute(string attrName)
        ////{
        ////    if (string.IsNullOrEmpty(attrName))
        ////        return null;

        ////    TScalarValueAttrValue attr = null;
        ////    using (var uow = new UnitOfWorkWithOAM())
        ////    {
        ////        //attr = uow.Context.TAttrValues.Where<TAttrValue>((e, i) => e.Object.Id == Id && e.Attribute.Name == attrName).FirstOrDefault<TAttrValue>() as TScalarValueAttrValue;
        ////    }
        ////    return attr != null ? attr.Value : null;
        ////}

        //public TObject GetScalarReferenceAttribute(string attrName)
        //{
        //    if (string.IsNullOrEmpty(attrName))
        //        return null;

        //    TReferenceScalarAttrValue attr;
        //    using (var uow = new UnitOfWorkWithOAM())
        //    {
        //        attr = uow.Context.TAttrValues.Where<TAttrValue>((e, i) => e.Object.Id == Id && e.Attribute.Name == attrName).FirstOrDefault<TAttrValue>() as TReferenceScalarAttrValue;
        //    }
        //    return attr != null ? attr.Value : null;
        //}

        //public IEnumerable<string> GetVectorAttribute(string attrName)
        //{
        //    if (string.IsNullOrEmpty(attrName))
        //        return null;

        //    List<string> values = null;

        //    using (var uow = new UnitOfWorkWithOAM())
        //    {
        //        var attr = uow.Context.TAttrValues.Where<TAttrValue>((e, index) => e.Object.Id == Id && e.Attribute.Name == attrName).FirstOrDefault<TAttrValue>() as TVectorAttrValue;
        //        //values = attr == null ? null : attr.Values.Select<TListValues, string>((e, i) => e.Value).ToList<string>();
        //    }
        //    return values;
        //}

        //public IEnumerable<TObject> GetVectorReferenceAttribute(string attrName)
        //{
        //    if (string.IsNullOrEmpty(attrName))
        //        return null;

        //    List<TObject> values = null;
        //    using (var uow = new UnitOfWorkWithOAM())
        //    {
        //        var attr = uow.Context.TAttrValues.Where<TAttrValue>((e, i) => e.Object.Id == Id && e.Attribute.Name == attrName) as TReferenceVectorAttrValue;
        //        values = attr == null ? null : attr.Values.ToList<TObject>();
        //    }
        //    return values;
        //}

        #region .ctor

        public TObject() { }

        public TObject(UnitOfWorkWithOAM uow)
        {
            UoW = uow;
        }

        #endregion .ctor

        #region IUoWConnectable

        private UnitOfWorkWithOAM _uow;
        public UnitOfWorkWithOAM UoW
        {
            get
            {
                return _uow;
            }
            set
            {
                _uow = value;
            }
        }

        #endregion IUoWConnectable
    }
}
