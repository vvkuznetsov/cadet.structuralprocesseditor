﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ObjectAttributeMapping.OAM.OAMController;

namespace ObjectAttributeMapping.OAM.OAMDB
{
    public partial class TAttribute
    {
        public TAttrValue CreateAttributeValue()
        {
            return TAttrValue.CreateInstance(AttrType);
        }

        public AttributeType AttrType
        {
            get 
            {
                return (AttributeType)AttrTypeNumber;
            }
            set 
            {
                AttrTypeNumber = (byte)value;                
            }
        }

        #region .ctor

        public TAttribute() { } 

        public TAttribute(string name = "")
        {
            Name = name;
        }

        public TAttribute(string name, AttributeType type) : this(name)
        {
            AttrType = type;
        }

        #endregion .ctor
    }
}
