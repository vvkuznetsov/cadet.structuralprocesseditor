﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ObjectAttributeMapping.OAM.OAMController;
using System.Data.Objects;

namespace ObjectAttributeMapping.OAM.OAMDB
{
    /// <summary>
    /// Тип OAM
    /// </summary>
    public partial class TType
    {
        #region Названия базовых типов

        public static readonly string TypeNameNamedType = "named_type";
        public static readonly string TypeNamedTypeAttrNameName = "named_type_name";

        public static readonly string TypeNameStruct = "struct";
        public static readonly string TypeStructAttrNameInputInterfaces = "struct_input_interfaces";
        public static readonly string TypeStructAttrNameOutputInterfaces = "struct_output_interfaces";
        public static readonly string TypeStructAttrNameParentDia = "struct_parent_dia";
        public static readonly string TypeStructAttrNameInnerDecompositionDia = "struct_inner_decomposition_dia";
        public static readonly string TypeStructAttrNameUpperLeftX = "struct_upper_left_x";
        public static readonly string TypeStructAttrNameUpperLeftY = "struct_upper_left_y";
        public static readonly string TypeStructAttrNameWidth = "struct_width";
        public static readonly string TypeStructAttrNameHeight = "struct_height";

        public static readonly string TypeNameInterface = "interface";
        public static readonly string TypeInterfaceAttrNameOwner = "interface_owner";
        public static readonly string TypeInterfaceAttrNamePositionX = "interface_position_x";
        public static readonly string TypeInterfaceAttrNamePositionY = "interface_position_y";

        public static readonly string TypeNameConnection = "connection";
        public static readonly string TypeConnectionAttrNameHeadInterface = "connection_head_interface";
        public static readonly string TypeConnectionAttrNameTailInterface = "connection_tail_inteface";

        public static readonly string TypeNameDiagramm = "diagramm";
        public static readonly string TypeDiagrammAttrNameElements = "diagramm_elements";

        public static readonly string TypeNameFunction = "function";
        public static readonly string TypeFunctionAttrNameInput = "function_input";
        public static readonly string TypeFunctionAttrNameOutput = "function_output";

        #endregion Названия базовых типов

        #region Ключи конфигурации

        public static readonly string ConfigKeyOriginalTypesCreated = "IsOriginalTypesCreated";

        #endregion Ключи конфигурации

        /// <summary>
        /// Создает объект
        /// </summary>
        /// <param name="parent">родительский объект</param>
        /// <param name="children">дочерние объекты</param>
        /// <returns>созданный объект</returns>
        public TObject CreateObject(UnitOfWorkWithOAM uow)
        {
            TObject o = new TObject() { Type = this };
            uow.Context.AddToTObjects(o);

            foreach(var t in GetAttributes(uow,true))
            {
                var avalue = t.CreateAttributeValue();
                avalue.Object = o;
                avalue.Attribute = t;
                o.Attributes.Add(avalue);
                uow.Context.AddToTAttrValues(avalue);                
            }
            return o;
        }

        /// <summary>
        /// Создает новый тип
        /// </summary>
        /// <param name="name">имя типа</param>
        /// <param name="parentType">базовый тип</param>
        /// <param name="attrs">аттриубты типа</param>
        /// <returns>тип</returns>
        public static TType CreateType(string name, string[] parentTypesNames = null, IEnumerable<TAttribute> attrs = null)
        {
            TType t;
            using (var uow = new UnitOfWorkWithOAM())
            {
                t = new TType() { Name = name };
                if (parentTypesNames != null)
                {
                    var parents = new HashSet<string>(parentTypesNames);
                    //var parentsTypes = uow.Context.TTypes.Where<TType>((e) => parents.Contains(e.Name)).ToList();
                    //parentsTypes.ForEach((e) => t.Parents.Add(e));
                    t.Parent = uow.Context.TTypes.Where<TType>((e) => parents.Contains(e.Name)).FirstOrDefault();
                }
                if (attrs != null)
                {
                    foreach (var attr in attrs)
                    {
                        attr.Type = t;
                        t.Attributes.Add(attr);
                    }
                }
                uow.Context.AddToTTypes(t);
            }

            return t;
        }

        /// <summary>
        /// Возвращает все родительские типы
        /// </summary>
        /// <param name="uow">единица работы</param>
        /// <returns>родительские типы</returns>
        public IEnumerable<TType> GetLinearInheritanceParentTypes(UnitOfWorkWithOAM uow)
        {
            var types = uow.Context.TTypes.ToList();
            var parent = Parent;
            var parentTypes = new List<TType>();

            while (parent != null)
            {
                types.RemoveAll((e) => e.Id == parent.Id);
                parentTypes.Add(parent);
                parent = types.FirstOrDefault((e) => parent.Parent != null && e.Id == parent.Parent.Id);
            }

            return parentTypes;
        }

        /// <summary>
        /// Возвращает все родительские типы
        /// </summary>
        /// <returns>родительские типы</returns>
        public IEnumerable<TType> GetLinearInheritanceParentTypes()
        {
            using (var uow = new UnitOfWorkWithOAM())
            {
                return GetLinearInheritanceParentTypes(uow);
            }
        }

        /// <summary>
        /// Возвращает атрибуты типа
        /// </summary>
        /// <param name="uow">единица работы</param>
        /// <param name="includeParentAttributes">флаг включения атрибутов наследуемых типов</param>
        /// <returns>атрибуты</returns>
        public IEnumerable<TAttribute> GetAttributes(UnitOfWorkWithOAM uow, bool includeParentAttributes = false)
        {
            var attrs = new List<TAttribute>(Attributes);

            if (includeParentAttributes)
            {
                var types = GetLinearInheritanceParentTypes(uow);
                foreach (var t in types)
                {
                    attrs.AddRange(t.Attributes);
                }
            }
            return attrs;
        }

        /// <summary>
        /// Возвращает атрибуты типа
        /// </summary>
        /// <param name="includeParentAttributes">флаг включения атрибутов наследуемых типов</param>
        /// <returns>атрибуты</returns>
        public IEnumerable<TAttribute> GetAttributes(bool includeParentAttributes = false)
        {
            using (var uow = new UnitOfWorkWithOAM())
            {
                return GetAttributes(uow, includeParentAttributes);
            }
        }

        /// <summary>
        /// Возвращает наследников типа
        /// </summary>
        /// <param name="uow">кдиница работы</param>
        /// <param name="includeOriginalType">флаг включения исходного типа</param>
        /// <returns>наследники</returns>
        public IEnumerable<TType> GetInheritedTypes(UnitOfWorkWithOAM uow, bool includeOriginalType = true)
        {
            var inheritedTypes = new List<TType>();
            if (includeOriginalType)
            {
                inheritedTypes.Add(this);
            }
            var types = uow.Context.TTypes.ToList();
            var parents = new HashSet<TType>(){ this };

            do
            {
                var newParents = new HashSet<TType>();
                foreach (var t in types)
                {
                    if (parents.Contains(t.Parent))
                    {
                        newParents.Add(t);
                        inheritedTypes.Add(t);
                    }
                }
                types.RemoveAll((e) => newParents.Contains(e));
                parents = newParents;
            }
            while (parents.Count != 0);
            return inheritedTypes;
        }

        #region IEqualityComparer<TType> (для HashSet<TType>)

        public override bool  Equals(object obj)
        {
 	         if (!(obj is TType))
                 return false;
            return ((TType)obj).Id == Id;
        }

        public override int  GetHashCode()
        {
            return Id;
        }

        #endregion IEqualityComparer<TType>
    }
}
