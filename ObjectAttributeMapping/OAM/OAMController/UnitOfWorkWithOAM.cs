﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ObjectAttributeMapping.OAM.OAMDB;
using System.Data.Objects;

namespace ObjectAttributeMapping.OAM.OAMController
{
    /// <summary>
    /// Единица работы с ObjectAttributeMapping
    /// </summary>
    public class UnitOfWorkWithOAM : IDisposable
    {
        /// <summary>
        /// Контекст БД OAM
        /// </summary>
        private readonly OAMContainer _oamContext;

        /// <summary>
        /// Контекст БД OAM
        /// </summary>
        public OAMContainer Context
        {
            get
            {
                return _oamContext;
            }
        }

        /// <summary>
        /// Сохраняет изменения, произведенные в рамках единицы работы
        /// </summary>
        public void Commit()
        {
            int result = _oamContext.SaveChanges(SaveOptions.DetectChangesBeforeSave);
            _oamContext.AcceptAllChanges();
        }

        #region .ctor

        public UnitOfWorkWithOAM()
        {
            _oamContext = new OAMContainer();
        }

        public UnitOfWorkWithOAM(string connectionString)
        {
            _oamContext = new OAMContainer(connectionString);
        }

        #endregion .ctor

        #region IDisposable

        public void Dispose()
        {
            Commit();
        }

        #endregion IDisposable
    }
}
