﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectAttributeMapping.OAM.OAMController
{
    /// <summary>
    /// Типы атрибутов
    /// </summary>
    public enum AttributeType : byte
    {
        /// <summary>
        /// Одиночное значение
        /// </summary>
        ScalarValue         = 0,

        /// <summary>
        /// Набор значений
        /// </summary>
        VectorValue         = 1,

        /// <summary>
        /// Одиночная ссылка
        /// </summary>
        ScalarReference     = 2,

        /// <summary>
        /// Набор ссылок
        /// </summary>
        VectorReference     = 3
    }
}
