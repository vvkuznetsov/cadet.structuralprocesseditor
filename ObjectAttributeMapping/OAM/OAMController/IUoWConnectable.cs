﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ObjectAttributeMapping.OAM.OAMController
{
    /// <summary>
    /// Объект, который используется в рамках работы
    /// </summary>
    interface IUoWConnectable
    {
        UnitOfWorkWithOAM UoW { get; set; }
    }
}
