﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ObjectAttributeMapping.OAM.OAMDB;
using System.Data.Objects;

namespace ObjectAttributeMapping.OAM.OAMController
{
    /// <summary>
    /// Контроллер объектно-атрибутного отображения
    /// </summary>
    /// <remarks>позволяет управлять:
    /// 1. типами 
    /// 2. атрибутами типов
    /// 3. объектами
    /// 4. значениями атрибутов объектов</remarks>
    [Obsolete("Не использовать. Использовать UnitOfWorkWithOAM")]
    public class ObjectAttributeMappingController
    {
        /// <summary>
        /// Контекст БД OAM
        /// </summary>
        private readonly OAMContainer _oamContext;

        /// <summary>
        /// Контекст БД OAM
        /// </summary>
        public OAMContainer Context
        {
            get 
            {
                return _oamContext;
            }
        }

        #region Типы

        

        #endregion

        private void ConfirmChanges()
        {
            _oamContext.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
        }

        #region .ctor

        public ObjectAttributeMappingController()
        {
            _oamContext = new OAMContainer();
        }

        public ObjectAttributeMappingController(string connectionString)
        {
            _oamContext = new OAMContainer(connectionString);
        }

        #endregion
    }
}