﻿using System.Windows;

namespace StructuralProcessEditor.TextSelection.View.View
{
    /// <summary>
    /// Interaction logic for WordToolbarWindow.xaml
    /// </summary>
    public partial class WordToolbarWindow : Window
    {
        public WordToolbarWindow()
        {
            InitializeComponent();
        }
    }
}
