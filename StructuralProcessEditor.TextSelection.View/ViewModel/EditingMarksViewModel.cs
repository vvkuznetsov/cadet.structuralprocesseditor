﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Microsoft.Win32;
using StructuralProcessEditor.TextSelection.View.Behavior;
using StructuralProcessEditor.TextSelection.View.View;
using StructuralProcessEditor.TextSelection.View.ViewModel.Common;

namespace StructuralProcessEditor.TextSelection.View.ViewModel
{
    class EditingMarksViewModel : PropertyChangedBase, IDragAndDrop
    {
        public EditingMarksViewModel(List<SelectedTextRange> ranges)
        {
            var temp = ranges.Select(range => new TextRangeViewModel(this, range.Id, range.Start, range.End, range.Text));
            Ranges = new ObservableCollection<TextRangeViewModel>(temp);
        }

        public ObservableCollection<TextRangeViewModel> Ranges { get; set; }
        public TextRangeViewModel SelectedItem { get; set; }

        #region IDragAndDrop Members

        /// <summary>
        /// Returns the type of the item that can be dropped
        /// </summary>
        Type IDragAndDrop.DataType
        {
            get { return typeof(TextRangeViewModel); }
        }

        void IDragAndDrop.Rearrange(object data, int index)
        {
            var item = data as TextRangeViewModel;

            if (item == null)
                return;

            if (Ranges.IndexOf(item) < index)
            {
                index--;
            }

            if (item.Id == index + 1)
                return;

            Ranges.Remove(item);
            Ranges.Insert(index, item);

            for (int i = 0; i < Ranges.Count; i++)
            {
                if (Ranges[i].Id != i + 1)
                {
                    Ranges[i].Id = i + 1;
                }
            }

            RaisePropertyChanged("Ranges");

            SelectedItem = item;
        }

        #endregion

        public void Remove(TextRangeViewModel item)
        {
            if (item == null)
                return;

            Ranges.Remove(item);

            for (int i = 0; i < Ranges.Count; i++)
            {
                if (Ranges[i].Id != i + 1)
                {
                    Ranges[i].Id = i + 1;
                }
            }

            RaisePropertyChanged("Ranges");
        }
    }
}
