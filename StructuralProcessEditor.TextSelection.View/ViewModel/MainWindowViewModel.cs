﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using Microsoft.Win32;
using StructuralProcessEditor.TextSelection.View.View;
using StructuralProcessEditor.TextSelection.View.ViewModel.Common;

namespace StructuralProcessEditor.TextSelection.View.ViewModel
{
    class MainWindowViewModel : PropertyChangedBase
    {
        private bool _isVisibile;

        public static MainWindowViewModel Instance
        {
            get { return new MainWindowViewModel(); }
        }

        public List<SelectedTextRange> Ranges { get; set; }

        private static string FileSelect()
        {
            var dlg = new Microsoft.Win32.OpenFileDialog
                {
                    DefaultExt = ".doc;*.docx",
                    Filter = "Word Files (*.doc;*.docx)|*.doc;*.docx"
                };

            bool? result = dlg.ShowDialog();

            if (result == true)
            {
                return dlg.FileName;
            }

            return null;
        }

        public ICommand MarkDocument
        {
            get
            {
                return new DelegateCommand(arg =>
                    {
                        var filename = FileSelect();
                        if (filename == null)
                            return;

                        var window = new WordToolbarWindow { Topmost = true, DataContext = new WordToolbarViewModel(filename, SetMarketedRanges) };
                        
                        window.Closed += (sender, args) =>
                            {
                                IsVisible = true;
                            };

                        window.Show();
                        IsVisible = false;
                    });
            }
        }

        protected void SetMarketedRanges(List<SelectedTextRange> ranges)
        {
            Ranges = ranges;
        }

        public ICommand EditMarks
        {
            get
            {
                return new DelegateCommand(arg =>
                    {
                        if (Ranges == null)
                            return;

                        var window = new EditingMarksWindow { DataContext = new EditingMarksViewModel(Ranges) };

                        window.Closed += (sender, args) =>
                        {
                            IsVisible = true;
                        };

                        window.Show();
                        IsVisible = false;
                    });
            }
        }

        public bool IsVisible
        {
            get { return _isVisibile; }
            set
            {
                _isVisibile = value;
                base.RaisePropertyChanged("IsVisible");
            }
        }
    }
}
