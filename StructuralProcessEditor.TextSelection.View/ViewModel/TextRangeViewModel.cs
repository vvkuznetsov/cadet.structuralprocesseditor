﻿using System;
using System.Windows.Input;
using StructuralProcessEditor.TextSelection.View.Behavior;
using StructuralProcessEditor.TextSelection.View.ViewModel.Common;

namespace StructuralProcessEditor.TextSelection.View.ViewModel
{
    class TextRangeViewModel : PropertyChangedBase, IDragAndDrop
    {
        public TextRangeViewModel(EditingMarksViewModel parent, int id, int start, int end, string text)
        {
            Parent = parent;
            Id = id;
            Start = start;
            End = end;
            Text = text;
        }

        /// <summary>
        /// Kind of Madgic don't work here (EditingMarksWindow)
        /// </summary>
        private int _id;
        public int Id
        {
            get { return _id; }
            set
            {
                _id = value;
                RaisePropertyChanged("Id");
            }
        }

        public int Start { get; private set; }
        public int End { get; private set; }
        public string Text { get; private set; }
        public EditingMarksViewModel Parent { get; private set; }

        public ICommand Remove
        {
            get
            {
                return new DelegateCommand(arg =>
                {
                    if (Parent != null)
                    {
                        Parent.Remove(this);
                    }
                });
            }
        }

        #region IDragAndDrop Members

        /// <summary>
        /// Returns the type of the item that can be dropped
        /// </summary>
        Type IDragAndDrop.DataType
        {
            get { return typeof(TextRangeViewModel); }
        }

        void IDragAndDrop.Rearrange(object data, int index)
        {
            var item = data as TextRangeViewModel;

            if (item != null && Parent != null)
            {
                (Parent as IDragAndDrop).Rearrange(item, index);
            }
        }

        #endregion
    }
}
