﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using StructuralProcessEditor.TextSelection.View.ViewModel.Common;

namespace StructuralProcessEditor.TextSelection.View.ViewModel
{
    class WordToolbarViewModel : PropertyChangedBase
    {
        private WordWorker _word;
        private List<SelectedTextRange> _ranges;
        private readonly Action<List<SelectedTextRange>> _setMarketedRanges;

        public WordToolbarViewModel(string filename, Action<List<SelectedTextRange>> setMarketedRanges)
        {
            Open(filename);
            _setMarketedRanges = setMarketedRanges;
        }

        private void Open(string filename)
        {
            _word = new WordWorker();
            _word.Open(filename);
            _word.ClearMarking();
            _word.Show();
        }

        public ICommand SelectText
        {
            get
            {
                return new DelegateCommand(arg => _word.SelectText());
            }
        }

        public ICommand DeselectText
        {
            get
            {
                return new DelegateCommand(arg => _word.DeselectText());
            }
        }

        public ICommand Save
        {
            get
            {
                return new DelegateCommand(arg =>
                    {
                        _ranges = _word.GetHighlightedTextRanges();
                        _word.HighlightText(_ranges);
                    });
            }
        }

        #region Window

        public bool? DialogResult { get; set; }

        private bool CanCloseFrom()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();

            return MessageBoxResult.Yes == MessageBox.Show("Вы действительно хотите выйти?",
                    "Выход", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
        }

        public ICommand CloseCommand
        {
            get
            {
                return new DelegateCommand(() =>
                {
                    // Caution!!! Magic starts here
                    DialogResult = null;
                    DialogResult = true;
                    // Caution!!! Magic ends here

                    _setMarketedRanges(_ranges);
                    _word.Dispose();
                }, CanCloseFrom);
            }
        }

        public virtual ICommand CloseFailCommand
        {
            get
            {
                return new DelegateCommand(() => { });
            }
        }

        #endregion
    }
}
