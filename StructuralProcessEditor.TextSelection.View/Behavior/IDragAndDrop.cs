﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StructuralProcessEditor.TextSelection.View.Behavior
{
    interface IDragAndDrop
    {
        /// <summary>
        /// Type of the data item
        /// </summary>
        Type DataType { get; }

        /// <summary>
        /// Rearrange data in the collection.
        /// </summary>
        /// <param name="data">The data to be rearranged</param>
        /// <param name="index">The index location to insert the data</param>
        void Rearrange(object data, int index);
    }
}
