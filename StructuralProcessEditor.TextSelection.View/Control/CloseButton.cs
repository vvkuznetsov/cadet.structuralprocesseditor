﻿using System.Windows;

namespace StructuralProcessEditor.TextSelection.View.Control
{
	public class CloseButton : CaptionButton
	{
		static CloseButton()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(CloseButton), new FrameworkPropertyMetadata(typeof(CloseButton)));
		}

        protected override void OnClick()
        {
            base.OnClick();
            Window.GetWindow(this).Close();
        }
	}
}
