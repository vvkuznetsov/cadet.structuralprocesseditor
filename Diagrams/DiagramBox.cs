﻿/*************************************************************************
Diagrams library
Copyright (c) 2010 Pavel Torgashov.

>>> LICENSE >>>
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation (www.fsf.org); either version 2 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License is available at
http://www.fsf.org/licensing/licenses

>>> END OF LICENSE >>>
*************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Drawing.Printing;

namespace Diagnostics
{
    public partial class DiagramBox : UserControl
    {
        protected Diagram diagram;
        //выделенная фигура
        protected Figure selectedFigure = null;
        //фигура или маркер, который тащится мышью
        protected Figure draggedFigure = null;

        protected List<Marker> markers = new List<Marker>();
        protected Pen selectRectPen;
        protected ContextMenuStrip mainMenu;

        /// <summary>
        /// Контекстное меню (при отсутствии выделенного объекта)
        /// </summary>
        protected virtual ContextMenuStrip MainContextMenu
        {
            get
            {
                return cmMain;
            }
        }

        /// <summary>
        /// Контекстное меню (при наличии выделенного объекта)
        /// </summary>
        protected virtual ContextMenuStrip SelectedElementContextMenu
        {
            get 
            { 
                return null; 
            }
        }

        /// <summary>
        /// Контекстное меню по умолчанию (при наличии выделенного объекта)
        /// </summary>
        protected ContextMenuStrip DefaultSelectedElementContextMenu
        {
            get 
            {
                return cmSelectedFigure;
            }
        }

        protected ToolTip ToolTip1
        {
            get { return toolTip1; }
        }

        public DiagramBox()
        {
            InitializeComponent();

            DoubleBuffered = true;
            ResizeRedraw = true;

            selectRectPen = new Pen(Color.Red, 1);
            selectRectPen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
            mainMenu = cmSelectedFigure;
        }


        //диаграмма, отображаемая компонентом
        public Diagram Diagram
        {
            get { return diagram; }
            set { 
                diagram = value;
                selectedFigure = null;
                draggedFigure = null;
                markers.Clear();
                Invalidate();
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Draw(e.Graphics);
        }

        protected virtual void Draw(Graphics gr)
        {
            gr.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            if (Diagram != null)
            {
                //сначала рисуем соединительные линии
                foreach (Figure f in Diagram.figures)
                    if (f is LineFigure)
                        f.Draw(gr);
                //затем рисуем плоские фигуры
                foreach (Figure f in Diagram.figures)
                    if (f is SolidFigure)
                    {
                        if (f is ConnectorFigure) 
                        {
                           // bru
                        }
                        f.Draw(gr);
                    }
            }

            //рисуем прямоугольник выделенной фигуры
            if (selectedFigure is SolidFigure)
            {
                SolidFigure figure = selectedFigure as SolidFigure;
                RectangleF bounds = figure.Bounds;
                gr.DrawRectangle(selectRectPen, bounds.Left - 2, bounds.Top - 2, bounds.Width + 4, bounds.Height + 4);
            }
            //рисуем маркеры
            foreach (Marker m in markers)
                m.Draw(gr);
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            Focus();
            draggedFigure = FindFigureByPoint(e.Location);
            if (!(draggedFigure is Marker))
            {
                selectedFigure = draggedFigure;
                CreateMarkers();
            }
            startDragPoint = e.Location;
            Invalidate();

            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                if (selectedFigure == null)
                    MainContextMenu.Show(PointToScreen(e.Location));
                else
                {
                    if (SelectedElementContextMenu != null)
                    {
                        SelectedElementContextMenu.Show(PointToScreen(e.Location));
                    }
                    else
                    {
                        DefaultSelectedElementContextMenu.Show(PointToScreen(e.Location));
                    }                        
                }
            }
        }

        protected override void OnMouseDoubleClick(MouseEventArgs e)
        {
            base.OnMouseDoubleClick(e);
            if (selectedFigure != null)
                editTextToolStripMenuItem_Click(null, null);
        }

        protected void CreateMarkers()
        {
            if (selectedFigure == null)
                markers = new List<Marker>();
            else
            {
                markers = selectedFigure.CreateMarkers(Diagram);
                UpdateMarkers();
            }
        }

        protected void UpdateMarkers()
        {
            foreach (Marker m in markers)
                if(draggedFigure!=m)//маркер который тащится, обновляется сам
                    m.UpdateLocation();
        }

        protected Point startDragPoint;

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            Figure fig = FindFigureByPoint(e.Location);

            if ((!(fig is Marker)) && (fig != null))
            {
                //button1.Visible = true;
                toolTip1.ToolTipTitle = fig.GetType().ToString();
            }
            else 
            {

            }
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                if (draggedFigure != null && (draggedFigure is SolidFigure))
                {
                    if ((draggedFigure as SolidFigure).Connectors!=null)
                    {
                        foreach (ConnectorFigure con in (draggedFigure as SolidFigure).Connectors)
                        {
                            con.Offset(e.Location.X - startDragPoint.X, e.Location.Y - startDragPoint.Y);
                        }
                    }
                    (draggedFigure as SolidFigure).Offset(e.Location.X - startDragPoint.X, e.Location.Y - startDragPoint.Y);
                    UpdateMarkers();
                    Invalidate();
                }
            }
            else
            {
                Figure figure = FindFigureByPoint(e.Location);
                if (figure is Marker)
                    Cursor = Cursors.SizeAll;
                else
                if (figure != null)
                    Cursor = Cursors.Hand;
                else
                    Cursor = Cursors.Default;
            }

            startDragPoint = e.Location;
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            draggedFigure = null;
            UpdateMarkers();
            Invalidate();
        }

        //поиск фигуры, по данной точке
        protected Figure FindFigureByPoint(Point p)
        {
            //ищем среди маркеров
            foreach (Marker m in markers)
                if (m.IsInsidePoint(p))
                    return m;
            //затем ищем среди плоских фигур
            for (int i = Diagram.figures.Count - 1; i >= 0; i--)
                if (Diagram.figures[i] is SolidFigure && Diagram.figures[i].IsInsidePoint(p))
                    return Diagram.figures[i];
            //затем ищем среди линий
            for (int i = Diagram.figures.Count - 1; i >= 0; i--)
                if (Diagram.figures[i] is LineFigure && Diagram.figures[i].IsInsidePoint(p))
                    return Diagram.figures[i];
            return null;
        }

        protected void miAddRect_Click(object sender, EventArgs e)
        {
            AddFigure<RectFigure>(startDragPoint);
        }

        protected FigureType AddFigure<FigureType>(Point location) where FigureType : SolidFigure
        {
            if (Diagram == null)
                return null;
            FigureType figure = Activator.CreateInstance<FigureType>();
            figure.location = location;
            if (figure is ConnectorFigure) 
            {
                if ((selectedFigure as SolidFigure).Connectors == null)
                {
                    (selectedFigure as SolidFigure).Connectors = new List<ConnectorFigure>();
                }
                (selectedFigure as SolidFigure).Connectors.Add(figure as ConnectorFigure);
                //(figure as ConnectorFigure).parentFig = (selectedFigure as SolidFigure);
            }
            Diagram.figures.Add(figure);
            Invalidate();
            return figure;
        }

        protected void addRoundRectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddFigure<RoundRectFigure>(startDragPoint);
        }

        protected void addRhombToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddFigure<RhombFigure>(startDragPoint);
        }

        protected void addParalelogrammToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddFigure<ParalelogrammFigure>(startDragPoint);
        }

        protected void addEllipseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddFigure<EllipseFigure>(startDragPoint);
        }

        protected void addStackToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddFigure<StackFigure>(startDragPoint);
        }

        protected void addFrameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddFigure<FrameFigure>(startDragPoint);
        }

        protected void bringToFrontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (selectedFigure != null)
            {
                Diagram.figures.Remove(selectedFigure);
                Diagram.figures.Add(selectedFigure);
                Invalidate();
            }
        }

        protected void sendToBackToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (selectedFigure != null)
            {
                Diagram.figures.Remove(selectedFigure);
                Diagram.figures.Insert(0,selectedFigure);
                Invalidate();
            }
        }

        protected void editTextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (selectedFigure != null && (selectedFigure is SolidFigure))
            {
                SolidFigure figure = (selectedFigure as SolidFigure);
                TextBox textBox = new TextBox();
                textBox.Parent = this;
                textBox.SetBounds(figure.TextBounds.Left, figure.TextBounds.Top, figure.TextBounds.Width, figure.TextBounds.Height);
                textBox.Text = figure.text;
                textBox.Multiline = true;
                textBox.TextAlign = HorizontalAlignment.Center;
                textBox.Focus();
                textBox.LostFocus += new EventHandler(textBox_LostFocus);
            }
        }

        protected void textBox_LostFocus(object sender, EventArgs e)
        {
            if (selectedFigure != null && (selectedFigure is SolidFigure))
            {
                (selectedFigure as SolidFigure).text = (sender as TextBox).Text;
            }
            Controls.Remove((Control)sender);
        }

        protected void miAddLine_Click(object sender, EventArgs e)
        {
            if (selectedFigure != null && (selectedFigure is SolidFigure))
            {
                LineFigure line = new LineFigure();
                line.From = (selectedFigure as SolidFigure);
                EndLineMarker marker = new EndLineMarker(Diagram, 1);
                marker.location = line.From.location;
                marker.location.Offset(0, (int)line.From.Size.Height/2+10);
                line.To = marker;
                Diagram.figures.Add(line);
                selectedFigure = line;
                CreateMarkers();
                
                Invalidate();
            }
        }

        protected void miAddLedgeLine_Click(object sender, EventArgs e)
        {
            if (selectedFigure != null && (selectedFigure is SolidFigure))
            {
                LedgeLineFigure line = new LedgeLineFigure();
                line.From = (selectedFigure as SolidFigure);
                EndLineMarker marker = new EndLineMarker(Diagram, 1);
                marker.location = line.From.location;
                marker.location.Offset(0, (int)line.From.Size.Height / 2 + 10);
                line.To = marker;
                Diagram.figures.Add(line);
                selectedFigure = line;
                CreateMarkers();

                Invalidate();
            }
        }

        public void Delete(Figure figure)
        {
            if (figure != null)
            {
                //удалем фигуру
                Diagram.figures.Remove(figure);

                propertiesToolStripMenuItem.DropDownItems.Clear();

                //удялаем также все линии, ведущие к данной фигуре
                for (int i = Diagram.figures.Count - 1; i >= 0; i--)
                    if (Diagram.figures[i] is LineFigure)
                    {
                        LineFigure line = (Diagram.figures[i] as LineFigure);
                        if (line.To == figure || line.From == figure)
                            Diagram.figures.RemoveAt(i);
                    }

                figure = null;
                draggedFigure = null;
                CreateMarkers();

                Invalidate();
            }
        }

        protected void miDelete_Click(object sender, EventArgs e)
        {
            Delete(selectedFigure);
        }

        protected override void  OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
                miDelete_Click(null, null);

            base.OnKeyDown(e);
        }

        //преобразуем в картинку
        public Bitmap GetImage()
        {
            selectedFigure = null;
            draggedFigure = null;
            CreateMarkers();

            Bitmap bmp = new Bitmap(Bounds.Width, Bounds.Height);
            DrawToBitmap(bmp, new Rectangle(0, 0, bmp.Width, bmp.Height));

            return bmp;
        }


        //сохраняем как метафайл
        public void SaveAsMetafile(string fileName)
        {
            selectedFigure = null;
            draggedFigure = null;
            CreateMarkers();

            Metafile curMetafile = null;
            Graphics g = this.CreateGraphics();
            IntPtr hdc = g.GetHdc();
            Rectangle rect = new Rectangle(0, 0, 200, 200);
            try
            {
                curMetafile =
                    new Metafile(fileName, hdc, System.Drawing.Imaging.EmfType.EmfOnly);
            }
            catch
            {
                g.ReleaseHdc(hdc);
                g.Dispose();
                throw;
            }

            Graphics gr = Graphics.FromImage(curMetafile);
            //отрисовываем диаграмму
            Draw(gr);

            g.ReleaseHdc(hdc);
            gr.Dispose();
            g.Dispose();
        }

        protected void DiagramBox_Load(object sender, EventArgs e)
        {

        }

        protected void propertiesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //ToolStripMenuItem men = new ToolStripMenuItem("Новый");
            //propertiesToolStripMenuItem.DropDownItems.Add(men);
            new ObjectProperties { Text = "Hi" }.Show();
        }

        protected void добавитьТочкуToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        protected void добавитьТочкуСоединенияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (selectedFigure != null && (selectedFigure is SolidFigure)) 
            {
                //ConnectorFigure con = new ConnectorFigure();
                AddFigure<ConnectorFigure>(startDragPoint);
               //(selectedFigure as SolidFigure).Connectors += 1;
            }
        }
    }
}
