﻿namespace Diagnostics
{
    partial class DiagramBox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cmSelectedFigure = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miEditText = new System.Windows.Forms.ToolStripMenuItem();
            this.miAddLine = new System.Windows.Forms.ToolStripMenuItem();
            this.miAddLedgeLine = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.miDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.bringToFrontToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sendToBackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.добавитьТочкуСоединенияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.propertiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.miAddRect = new System.Windows.Forms.ToolStripMenuItem();
            this.addRoundRectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addRhombToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addParalelogrammToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addEllipseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addStackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addFrameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.видToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.стуктурнаяToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.функциональнаяToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.структурнофункциональнаяToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cmMain = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmSelectedFigure.SuspendLayout();
            this.cmMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmSelectedFigure
            // 
            this.cmSelectedFigure.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miEditText,
            this.miAddLine,
            this.miAddLedgeLine,
            this.toolStripMenuItem2,
            this.miDelete,
            this.toolStripMenuItem1,
            this.bringToFrontToolStripMenuItem,
            this.sendToBackToolStripMenuItem,
            this.добавитьТочкуСоединенияToolStripMenuItem,
            this.propertiesToolStripMenuItem});
            this.cmSelectedFigure.Name = "cmSelectedFigure";
            this.cmSelectedFigure.Size = new System.Drawing.Size(233, 192);
            // 
            // miEditText
            // 
            this.miEditText.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.miEditText.Name = "miEditText";
            this.miEditText.Size = new System.Drawing.Size(232, 22);
            this.miEditText.Text = "Редактировать текст";
            this.miEditText.Click += new System.EventHandler(this.editTextToolStripMenuItem_Click);
            // 
            // miAddLine
            // 
            this.miAddLine.Name = "miAddLine";
            this.miAddLine.Size = new System.Drawing.Size(232, 22);
            this.miAddLine.Text = "Добавить прямую линию";
            this.miAddLine.Click += new System.EventHandler(this.miAddLine_Click);
            // 
            // miAddLedgeLine
            // 
            this.miAddLedgeLine.Name = "miAddLedgeLine";
            this.miAddLedgeLine.Size = new System.Drawing.Size(232, 22);
            this.miAddLedgeLine.Text = "Добавить ломанную линию";
            this.miAddLedgeLine.Click += new System.EventHandler(this.miAddLedgeLine_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(229, 6);
            // 
            // miDelete
            // 
            this.miDelete.Name = "miDelete";
            this.miDelete.Size = new System.Drawing.Size(232, 22);
            this.miDelete.Text = "Удалить фигуру";
            this.miDelete.Click += new System.EventHandler(this.miDelete_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(229, 6);
            // 
            // bringToFrontToolStripMenuItem
            // 
            this.bringToFrontToolStripMenuItem.Name = "bringToFrontToolStripMenuItem";
            this.bringToFrontToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.bringToFrontToolStripMenuItem.Text = "Поместить на передний фон";
            this.bringToFrontToolStripMenuItem.Click += new System.EventHandler(this.bringToFrontToolStripMenuItem_Click);
            // 
            // sendToBackToolStripMenuItem
            // 
            this.sendToBackToolStripMenuItem.Name = "sendToBackToolStripMenuItem";
            this.sendToBackToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.sendToBackToolStripMenuItem.Text = "Поместить на задний фон";
            this.sendToBackToolStripMenuItem.Click += new System.EventHandler(this.sendToBackToolStripMenuItem_Click);
            // 
            // добавитьТочкуСоединенияToolStripMenuItem
            // 
            this.добавитьТочкуСоединенияToolStripMenuItem.Name = "добавитьТочкуСоединенияToolStripMenuItem";
            this.добавитьТочкуСоединенияToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.добавитьТочкуСоединенияToolStripMenuItem.Text = "Добавить точку соединения";
            this.добавитьТочкуСоединенияToolStripMenuItem.Click += new System.EventHandler(this.добавитьТочкуСоединенияToolStripMenuItem_Click);
            // 
            // propertiesToolStripMenuItem
            // 
            this.propertiesToolStripMenuItem.Name = "propertiesToolStripMenuItem";
            this.propertiesToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.propertiesToolStripMenuItem.Text = "Свойства";
            this.propertiesToolStripMenuItem.Click += new System.EventHandler(this.propertiesToolStripMenuItem_Click);
            // 
            // miAddRect
            // 
            this.miAddRect.Name = "miAddRect";
            this.miAddRect.Size = new System.Drawing.Size(295, 22);
            this.miAddRect.Text = "Добавить прямоугольник";
            this.miAddRect.Click += new System.EventHandler(this.miAddRect_Click);
            // 
            // addRoundRectToolStripMenuItem
            // 
            this.addRoundRectToolStripMenuItem.Name = "addRoundRectToolStripMenuItem";
            this.addRoundRectToolStripMenuItem.Size = new System.Drawing.Size(295, 22);
            this.addRoundRectToolStripMenuItem.Text = "Добавить скругленный прямоугольник ";
            this.addRoundRectToolStripMenuItem.Click += new System.EventHandler(this.addRoundRectToolStripMenuItem_Click);
            // 
            // addRhombToolStripMenuItem
            // 
            this.addRhombToolStripMenuItem.Name = "addRhombToolStripMenuItem";
            this.addRhombToolStripMenuItem.Size = new System.Drawing.Size(295, 22);
            this.addRhombToolStripMenuItem.Text = "Добавить ромб";
            this.addRhombToolStripMenuItem.Click += new System.EventHandler(this.addRhombToolStripMenuItem_Click);
            // 
            // addParalelogrammToolStripMenuItem
            // 
            this.addParalelogrammToolStripMenuItem.Name = "addParalelogrammToolStripMenuItem";
            this.addParalelogrammToolStripMenuItem.Size = new System.Drawing.Size(295, 22);
            this.addParalelogrammToolStripMenuItem.Text = "Добавить параллелограм";
            this.addParalelogrammToolStripMenuItem.Click += new System.EventHandler(this.addParalelogrammToolStripMenuItem_Click);
            // 
            // addEllipseToolStripMenuItem
            // 
            this.addEllipseToolStripMenuItem.Name = "addEllipseToolStripMenuItem";
            this.addEllipseToolStripMenuItem.Size = new System.Drawing.Size(295, 22);
            this.addEllipseToolStripMenuItem.Text = "Добавить круг";
            this.addEllipseToolStripMenuItem.Click += new System.EventHandler(this.addEllipseToolStripMenuItem_Click);
            // 
            // addStackToolStripMenuItem
            // 
            this.addStackToolStripMenuItem.Name = "addStackToolStripMenuItem";
            this.addStackToolStripMenuItem.Size = new System.Drawing.Size(295, 22);
            this.addStackToolStripMenuItem.Text = "Add Stack";
            this.addStackToolStripMenuItem.Click += new System.EventHandler(this.addStackToolStripMenuItem_Click);
            // 
            // addFrameToolStripMenuItem
            // 
            this.addFrameToolStripMenuItem.Name = "addFrameToolStripMenuItem";
            this.addFrameToolStripMenuItem.Size = new System.Drawing.Size(295, 22);
            this.addFrameToolStripMenuItem.Text = "Add Frame";
            this.addFrameToolStripMenuItem.Click += new System.EventHandler(this.addFrameToolStripMenuItem_Click);
            // 
            // видToolStripMenuItem
            // 
            this.видToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.стуктурнаяToolStripMenuItem,
            this.функциональнаяToolStripMenuItem,
            this.структурнофункциональнаяToolStripMenuItem});
            this.видToolStripMenuItem.Name = "видToolStripMenuItem";
            this.видToolStripMenuItem.Size = new System.Drawing.Size(295, 22);
            this.видToolStripMenuItem.Text = "Вид";
            // 
            // стуктурнаяToolStripMenuItem
            // 
            this.стуктурнаяToolStripMenuItem.Name = "стуктурнаяToolStripMenuItem";
            this.стуктурнаяToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.стуктурнаяToolStripMenuItem.Text = "Стуктурная";
            // 
            // функциональнаяToolStripMenuItem
            // 
            this.функциональнаяToolStripMenuItem.Name = "функциональнаяToolStripMenuItem";
            this.функциональнаяToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.функциональнаяToolStripMenuItem.Text = "Функциональная";
            // 
            // структурнофункциональнаяToolStripMenuItem
            // 
            this.структурнофункциональнаяToolStripMenuItem.Name = "структурнофункциональнаяToolStripMenuItem";
            this.структурнофункциональнаяToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.структурнофункциональнаяToolStripMenuItem.Text = "Структурнофункциональная";
            // 
            // cmMain
            // 
            this.cmMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miAddRect,
            this.addRoundRectToolStripMenuItem,
            this.addRhombToolStripMenuItem,
            this.addParalelogrammToolStripMenuItem,
            this.addEllipseToolStripMenuItem,
            this.addStackToolStripMenuItem,
            this.addFrameToolStripMenuItem,
            this.видToolStripMenuItem});
            this.cmMain.Name = "cmMain";
            this.cmMain.Size = new System.Drawing.Size(296, 180);
            // 
            // DiagramBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DiagramBox";
            this.Size = new System.Drawing.Size(541, 284);
            this.Load += new System.EventHandler(this.DiagramBox_Load);
            this.cmSelectedFigure.ResumeLayout(false);
            this.cmMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip cmSelectedFigure;
        private System.Windows.Forms.ToolStripMenuItem sendToBackToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bringToFrontToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem miEditText;
        private System.Windows.Forms.ToolStripMenuItem miAddLine;
        private System.Windows.Forms.ToolStripMenuItem miDelete;
        private System.Windows.Forms.ToolStripMenuItem miAddLedgeLine;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripMenuItem propertiesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem добавитьТочкуСоединенияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miAddRect;
        private System.Windows.Forms.ToolStripMenuItem addRoundRectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addRhombToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addParalelogrammToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addEllipseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addStackToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addFrameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem видToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem стуктурнаяToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem функциональнаяToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem структурнофункциональнаяToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip cmMain;
    }
}
