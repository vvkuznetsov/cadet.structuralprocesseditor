﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Word = Microsoft.Office.Interop.Word;

namespace StructuralProcessEditor.TextSelection
{
    public class Constants
    {
        public static readonly char[] TrimChars = { '\r', '\n', '\a', ' ' };

        public static object Missing = System.Reflection.Missing.Value;
        public static object Page = "\\Page";
        public static object StartOfDoc = "\\StartOfDoc";
        public static object EndOfDoc = "\\EndOfDoc";

        public static object CollapseEnd = Word.WdCollapseDirection.wdCollapseEnd;
        public static object PageBreak = Word.WdBreakType.wdPageBreak;
        public static object ColumnBreak = Word.WdBreakType.wdColumnBreak;
        public static object LineBreak = Word.WdBreakType.wdLineBreak;

        public static Word.WdOrientation HorizontalOrientation = Word.WdOrientation.wdOrientLandscape;
        public static Word.WdOrientation VerticalOrientation = Word.WdOrientation.wdOrientPortrait;

        public static Word.WdRowHeightRule RowHeightRule = Word.WdRowHeightRule.wdRowHeightExactly;
        public static Word.WdRulerStyle ColumnWidthRule = Word.WdRulerStyle.wdAdjustNone;
    }
}
