﻿namespace StructuralProcessEditor.TextSelection
{
    public class SelectedTextRange
    {
        public SelectedTextRange(int id, int start, int end, string text)
        {
            Id = id;
            Start = start;
            End = end;
            Text = text;
        }

        public int Id { get; private set; }
        public int Start { get; private set; }
        public int End { get; private set; }
        public string Text { get; private set; }
    }
}
