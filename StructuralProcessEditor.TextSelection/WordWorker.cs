﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Word = Microsoft.Office.Interop.Word;

namespace StructuralProcessEditor.TextSelection
{
    public partial class WordWorker : IDisposable
    {
        private Word._Application _app;
        private Word._Document _doc;

        #region PageSetup

        public Word.WdOrientation Orientation
        {
            get { return _doc.PageSetup.Orientation; }
            set { _doc.PageSetup.Orientation = value; }
        }

        #endregion


        #region Open & Create

        public void Create()
        {
            Dispose();

            _app = new Word.Application();
            _doc = _app.Documents.Add(ref Constants.Missing, ref Constants.Missing, ref Constants.Missing,
                                      ref Constants.Missing);
        }

        public void Open(string fileName)
        {
            Dispose();

            _app = new Word.Application();
            _doc = _app.Documents.Open(fileName);
        }

        #endregion




        #region Dispose

        public void Dispose()
        {
            try
            {
                if (_doc != null)
                    _doc.Close(SaveChanges: Word.WdSaveOptions.wdDoNotSaveChanges);

                if (_app != null)
                    _app.Quit();
            }
            catch (Exception)
            {
            }
        }

        #endregion


        public void CreateParagraph(int spaceAfter = 6, bool paragraphAfter = false, int fontSize = 15)
        {
            object range = _doc.Bookmarks.get_Item(Constants.EndOfDoc).Range;
            Word.Paragraph para = _doc.Content.Paragraphs.Add(ref range);
            para.Format.SpaceAfter = spaceAfter;
            para.Range.Font.Size = 1;

            if (paragraphAfter)
                para.Range.InsertParagraphAfter();
        }

        public void EndPage()
        {
            var range = _doc.Bookmarks.get_Item(Constants.EndOfDoc).Range;

            //// Move to the end
            //range.Collapse(Microsoft.Office.Interop.Word.WdCollapseDirection.wdCollapseEnd);
            //// Now add something behind the table to prevent word from joining tables into one
            //range.InsertParagraphAfter();
            //// gosh need to move to the end again
            //range.Collapse(Microsoft.Office.Interop.Word.WdCollapseDirection.wdCollapseEnd);

            range.Collapse(ref Constants.CollapseEnd);
            range.InsertBreak(ref Constants.ColumnBreak);
            range.Collapse(ref Constants.CollapseEnd);
        }

        public static void EndPage(Word.Range range)
        {
            range.Collapse(ref Constants.CollapseEnd);
            range.InsertBreak(ref Constants.ColumnBreak);
            range.Collapse(ref Constants.CollapseEnd);
        }

        public static void EndLine(Word.Range range)
        {
            range.Collapse(ref Constants.CollapseEnd);
            range.InsertBreak(ref Constants.LineBreak);
            range.Collapse(ref Constants.CollapseEnd);
        }

        public void Show()
        {
            _app.Visible = true;
        }

        public void SelectText()
        {
            var range = _app.Selection.Range;
            range.Font.Color = Word.WdColor.wdColorDarkYellow;
        }

        public void DeselectText()
        {
            _app.Selection.Range.Font.Color = Word.WdColor.wdColorBlack;
        }

        public void ClearMarking()
        {
            _doc.Content.Font.Color = Word.WdColor.wdColorBlack;
        }

        public List<SelectedTextRange> GetHighlightedTextRanges()
        {
            var ranges = new List<SelectedTextRange>();

            foreach (Word.Range storyRange in _doc.StoryRanges)
            {
                var range = storyRange;
                while (range != null)
                {
                    GetHighlightedTextRanges(range, ranges);

                    if (range.ShapeRange.Count > 0)
                    {
                        foreach (Word.Shape shape in range.ShapeRange)
                        {
                            if (shape.TextFrame.HasText != 0)
                            {
                                GetHighlightedTextRanges(
                                    shape.TextFrame.TextRange, ranges);
                            }
                        }
                    }

                    range = range.NextStoryRange;
                }
            }

            return ranges;
        }

        private static void GetHighlightedTextRanges(Word.Range range, List<SelectedTextRange> ranges)
        {

            range.Find.ClearFormatting();
            range.Find.ClearAllFuzzyOptions();
            range.Find.Font.Color = Word.WdColor.wdColorDarkYellow;
            range.Find.Wrap = Word.WdFindWrap.wdFindContinue;
            range.Find.Replacement.ClearFormatting();
            range.Find.Replacement.Font.Color = Word.WdColor.wdColorBlack;
            ;

            while (range.Find.Execute(Replace: Word.WdReplace.wdReplaceOne))
            {
                ranges.Add(new SelectedTextRange(ranges.Count + 1, range.Start, range.End, range.Text));
            }
            
        }

        public void HighlightText(List<SelectedTextRange> ranges)
        {
            foreach (var range in ranges)
            {
                _doc.Range(range.Start, range.End).Font.Color = Word.WdColor.wdColorDarkYellow;
            }
        }
    }
}
