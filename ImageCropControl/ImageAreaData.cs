﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;

namespace ImageCropControl
{
    public class ImageAreaData
    {
        // Путь до файла изображения
        public string ImagePath = null;
        // TODO: идентификатор изображения из БД (если уже загружено)
        public int ImageId = 0;

        // Координаты прямоугольника выделения относительно края изображения
        public int RectX = 0,
                   RectY = 0,
                   RectWidth = 0,
                   RectHeight = 0;

        // Вырезанный кусок картинки в виде картинки - можно писать в БД
        public BitmapImage Fragment = null;
    }
}
